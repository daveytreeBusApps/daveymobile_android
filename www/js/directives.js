 angular.module('starter.directives', [])
 .directive('imageonload', ['$timeout',function($timeout) {
    return {
        restrict: 'A',
        replace: false,
        link: function(scope, element) {
          element.on('load', function() {
              element.addClass('showImage')
          });
        }
    };
}])

.directive('ngInitial', function() {
  return {
    restrict: 'A',
    controller: [
      '$scope', '$element', '$attrs', '$parse', function($scope, $element, $attrs, $parse) {
        var getter, setter, val;
        val = $attrs.ngInitial || $attrs.value;
        getter = $parse($attrs.ngModel);
        setter = getter.assign;
        setter($scope, val);
      }
    ]
  };
})


.directive('m22Ripple', function () {
  function getCoords(evt, parent) {
    var containerX = parent.offsetLeft;
    var containerY = parent.offsetTop;

    var coord = {
      x: null,
      y: null
    };
    var isTouchSupported = 'ontouchstart' in window;
    if (isTouchSupported) {
      // for touch devices
      coord.x = evt.clientX - containerX;
      coord.y = evt.clientY - containerY;
      return coord;
    } else if (evt.offsetX || evt.offsetX == 0) {
      //for webkit browser like safari and chrome
      coord.x = evt.offsetX;
      coord.y = evt.offsetY;
      return coord;
    } else if (evt.layerX || evt.layerX == 0) {
      // for mozilla firefox
      coord.x = evt.layerX;
      coord.y = evt.layerY;
      return coord;
    }
  }

  return {
    restrict: 'A',
    compile: function compile(element, attributes) {
      var global = !angular.isUndefined(attributes.m22RippleGlobal);
      var parent = global ? angular.element(document).find('body') : element;
      var activeRipples = 0;
      element.on('click', function (e) {
        activeRipples++;
        parent.addClass('m22-ripple_active');
        var ripple = angular.element('<div class="m22-ripple"></ripple>');
        var coords = global ? { x: e.pageX, y: e.pageY } : getCoords(e, parent[0]);
        ripple.css({
          left: coords.x + 'px',
          top: coords.y + 'px',
          background: attributes.m22RippleColor || 'white'
        });
        parent.append(ripple);

        setTimeout(function () {

          ripple.remove();

          if (activeRipples > 0) {
            activeRipples--;
          } else if (activeRipples == 0) {
            parent.removeClass('m22-ripple_active');
          }
        }, 1000); // important to set timeout shorter the CSS animation duration
      });
    }
  };
})

.directive('myProgressBar', function() {
    return {
      restrict: 'E',
      scope: {
        myMaxStep: '@',
        myCurrentStep: '@',
        myShowSteps: '@'
      },
      templateUrl: 'templates/progress_bar.html',
      link: function(scope, element) {
        var drawProgressBar, progressStatusPixelWidth, progressTotalPixelWidth, setShowSteps, tryToInitProgressBar;
        setShowSteps = function() {
          scope.showSteps = (scope.myShowSteps != null) && scope.myShowSteps !== void 0;
          if (scope.showSteps) {
            return element.find('.progress').addClass('with-steps');
          }
        };
        progressTotalPixelWidth = function() {
          return Math.floor(element.children().width() * 100) / 100;
        };
        progressStatusPixelWidth = function() {
          return progressTotalPixelWidth() * scope.progressPercentage;
        };
        drawProgressBar = function() {
          setShowSteps();
          scope.progressPercentage = scope.myCurrentStep / scope.myMaxStep;
          return scope.progressBarPositionOffset = progressStatusPixelWidth();
        };
        tryToInitProgressBar = function() {
          if ((scope.myMaxStep != null) && (scope.myCurrentStep != null) && progressTotalPixelWidth() > 0) {
            return drawProgressBar();
          }
        };
        scope.$watch('myMaxStep', function() {
          if (scope.myMaxStep != null) {
            scope.myMaxStep = parseFloat(scope.myMaxStep);
          }
          return tryToInitProgressBar();
        });
        scope.$watch('myCurrentStep', function() {
          if (scope.myCurrentStep != null) {
            scope.myCurrentStep = parseFloat(scope.myCurrentStep);
          }
          return tryToInitProgressBar();
        });
        scope.$watch(progressTotalPixelWidth, function(newValue, oldValue) {
          if (newValue !== oldValue) {
            return tryToInitProgressBar();
          }
        });
        if (scope.myCurrentStep > 0) {
          return window.scope = scope;
        }
      }
    };
  })

.directive('ionToggleText', function () {

    var $ = angular.element;

  return {
    restrict: 'A',
    link: function ($scope, $element, $attrs) {
      
      // Try to figure out what text values we're going to use 
      
      var textOn = $attrs.ngTrueValue || 'on',
        textOff = $attrs.ngFalseValue || 'off';

      if ($attrs.ionToggleText) {
        var x = $attrs.ionToggleText.split(';');

        if (x.length === 2) {
          textOn = x[0] || textOn;
          textOff = x[1] || textOff;
        }
      }

      // Create the text elements
      
      var $handleTrue = $('<div class="handle-text handle-text-true">' + textOn + '</div>'),
        $handleFalse = $('<div class="handle-text handle-text-false">' + textOff + '</div>');

      var label = $element.find('label');

      if (label.length) {
        label.addClass('toggle-text');

        // Locate both the track and handle elements
        
        var $divs = label.find('div'),
          $track, $handle;

        angular.forEach($divs, function (div) {
          var $div = $(div);

          if ($div.hasClass('handle')) {
            $handle = $div;
          } else if ($div.hasClass('track')) {
            $track = $div;
          }
        });

        if ($handle && $track) {
          
          // Append the text elements
          
          $handle.append($handleTrue);
          $handle.append($handleFalse);

          // Grab the width of the elements
          
          var wTrue = $handleTrue[0].offsetWidth,
            wFalse = $handleFalse[0].offsetWidth;

          // Adjust the offset of the left element
          
          $handleTrue.css('left', '-' + (wTrue + 10) + 'px');

          // Ensure that the track element fits the largest text
          
          var wTrack = Math.max(wTrue, wFalse);
          $track.css('width', (wTrack + 60) + 'px');
        }
      }
    }
  }
})

.directive('map', ['$ionicPlatform', '$state','$rootScope', function ($ionicPlatform, $state,$rootScope){
    return {
      restrict: 'A',
      replace: false,
      link: function (scope, element, attrs) {

      var map = L.map(element[0],  { zoomControl: false, maxBounds:scope.bounds}).setView(scope.centerMap, scope.startzoom);

      var tilesUrl = 'http://api.tiles.mapbox.com/v4/mapbox.outdoors/{z}/{x}/{y}@2x.png?access_token=pk.eyJ1IjoidGhlc291cmF2IiwiYSI6ImNqZjFkbnM0ejB6OWsyeHFvY3ByNmFob3IifQ.drM4opDwgKniFXm-4FoZ4w'
      var tileLayerOnLine = L.tileLayer(tilesUrl,{minZoom:scope.minzoom, maxZoom: scope.maxzoom});
      var tileLayerOffLine = new L.TileLayer('Tiles/{z}/{x}/{y}.png', {minZoom:scope.minzoom, maxZoom: scope.maxzoom, detectRetina: false,unloadInvisibleTiles:true});

      if(!$rootScope.online){
        tileLayerOffLine.addTo(map);
      }else{
        tileLayerOnLine.addTo(map);
      }

      var features = []
      scope.projects.forEach(function(d){
        features.push(turf.point([d.lon, d.lat],d))
      })


      var fc = turf.featurecollection(features)

      var geoJsonLayer = L.geoJson(fc,{
        pointToLayer: function(feature, latlng) {
          var icon = L.icon({
              iconUrl: 'img/marker/' + feature.properties.category +'.png',
              iconRetinaUrl: 'img/marker/' + feature.properties.category +'@2x.png',
              iconSize: [25, 39],
              iconAnchor: [12, 39],
              shadowUrl: 'img/marker/marker-shadow.png'
          });

           return L.marker(latlng, {icon: icon});
        },
        onEachFeature: function (feature, layer) {
         layer.on("click", function(){
           scope.showFilters = false;
           map.panTo([feature.geometry.coordinates[1], feature.geometry.coordinates[0]])

           scope.selectedProject.name = feature.properties.name;
           scope.selectedProject.img = feature.properties.img;
           scope.selectedProject.lat = feature.properties.lat;
           scope.selectedProject.lon = feature.properties.lon;
           scope.selectedProject.id = feature.properties.id;
           scope.selectedProject.category = feature.properties.category;
           scope.selectedProject.address = feature.properties.adress;

            if(!scope.$$phase) {
                scope.$apply()
             }
         })
       }
     }).addTo(map);

   var iconMe = L.icon({
       iconUrl: 'img/marker/me.png',
       iconRetinaUrl: 'img/marker/me@2x.png',
       iconSize: [50, 50]
   });

    var me = L.marker([scope.latLng.lat, scope.latLng.lng],{icon: iconMe, zIndexOffset:1000})

    me.addTo(map)

    if($state.current.name=='around'){
      map.setView([scope.latLng.lat, scope.latLng.lng]);
    }

    scope.$watch('latLng', function(newValue, oldValue){

      if(newValue != oldValue){
        me.setLatLng([newValue.lat, newValue.lng])
        if($state.current.name=='around'){
          map.setView([newValue.lat, newValue.lng]);
        }
      }
    }, true)

    scope.$watch('filtersList', function(newValue, oldValue){
      if(newValue != oldValue){
        var hide = newValue.filter(function(d){return !d.checked}).map(function(d){return d.class})
        geoJsonLayer.eachLayer(function(layer, feature){

          if(hide.indexOf(layer.feature.properties.category) > -1){
            layer.setOpacity(0)
          }else{
            layer.setOpacity(1)
          }
        })
      }
    }, true)

    $rootScope.$watch('online', function(newValue, oldValue){
      if(newValue != oldValue){
        if(newValue){
          map.removeLayer(tileLayerOffLine)
          tileLayerOnLine.addTo(map);
        }else{
          map.removeLayer(tileLayerOnLine)
          tileLayerOffLine.addTo(map);
        }
      }

    })

    scope.$watch('center', function(newValue, oldValue){
      if(newValue != oldValue){
          map.setView([scope.latLng.lat, scope.latLng.lng],scope.maxzoom)
      }
    })

    }//end link
    };
  }])

 .directive('mapSingle', ['$rootScope',function ($rootScope){
    return {
      restrict: 'A',
      replace: false,
      link: function (scope, element, attrs) {
        
      

      var latitude, longitude;
      console.log("latitude and longitude from API", scope.selectedClient.latitude);
      latitude = scope.selectedClient.latitude;
      longitude = scope.selectedClient.longitude;
      console.log("Final Lat", latitude);
      console.log("Final Long", longitude);

      var map = L.map(element[0],  { zoomControl: true });
      map.resize();
      map.setView([latitude, longitude], scope.startzoom);
    
      var mapboxUrl = 'http://api.tiles.mapbox.com/v4/mapbox.outdoors/{z}/{x}/{y}@2x.png?access_token=pk.eyJ1IjoidGhlc291cmF2IiwiYSI6ImNqZ21mb3RydjFldWcycXQ0bTgyeW90YWcifQ.i3m3xk4iLYk6DvhrMtzSOQ';
      var osmUrl='https://a.tile.openstreetmap.org/{z}/{x}/{y}.png';
      var osmAttrib='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors';
      
      //var tilesUrl = 'http://api.tiles.mapbox.com/v4/mapbox.outdoors/{z}/{x}/{y}@2x.png?access_token=pk.eyJ1IjoidGhlc291cmF2IiwiYSI6ImNqZ21mb3RydjFldWcycXQ0bTgyeW90YWcifQ.i3m3xk4iLYk6DvhrMtzSOQ'
     // var tileLayerOnLine = L.tileLayer(tilesUrl);
      var tileLayerOnLine = new L.TileLayer(osmUrl, {minZoom: 4, maxZoom: 18, attribution: osmAttrib}); 
      var tileLayerOffLine = new L.TileLayer('Tiles/{z}/{x}/{y}.png', {minZoom:7, maxZoom: 15, detectRetina: false,unloadInvisibleTiles:true});

      var layers = [];
      map.eachLayer(function(layer) {
        if( layer instanceof L.TileLayer )
            layers.push(layer);
      });

      console.log("Initial Layers Array",layers);

      $rootScope.online = true;
      if(!$rootScope.online){
        tileLayerOffLine.addTo(map);
      }else{

         tileLayerOnLine.addTo(map);
         
      }
     
      var icon = L.icon({
          iconUrl: 'img/marker/' + scope.selectedClient.custType +'.png',
          iconRetinaUrl: 'img/marker/' + scope.selectedClient.custType +'@2x.png',
          iconSize: [25, 39],
          iconAnchor: [12, 39],
          shadowUrl: 'img/marker/marker-shadow.png'
      });


      L.marker([latitude,longitude], {icon: icon}).addTo(map);
     

      // Disable drag and zoom handlers.
      map.dragging.disable();
      map.touchZoom.disable();
      map.doubleClickZoom.disable();
      map.scrollWheelZoom.disable();

      // Disable tap handler, if present.
      if (map.tap) map.tap.disable();

          $rootScope.$watch('online', function(newValue, oldValue){
        if(newValue != oldValue){
          if(newValue){
            map.removeLayer(tileLayerOffLine)
            tileLayerOnLine.addTo(map);
          }else{
            map.removeLayer(tileLayerOnLine)
            tileLayerOffLine.addTo(map);
          }
        }

      })
  
     

    }//end link
    };
  }])