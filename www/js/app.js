// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic','starter.controllers','starter.services',
  'starter.filters','starter.routes','starter.directives','ngCordova','angular-jwt',
  'ngCordova.plugins.nativeStorage','ngLetterAvatar', 'jett.ionic.filter.bar','ngAvatar','ion-sticky',
  'ngStorage','ti-segmented-control','ngAnimate','ion-floating-menu','ionicRipple','ngMaterial','ngMessages','ngAria','ui-leaflet'])

.run(function($ionicPlatform,$cordovaNetwork,$timeout,$ionicPopup,$state) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }


      window.ga.startTrackerWithId('UA-89296115-1', 10);
      console.log("ga tracker initialized");
      window.ga.setAppVersion('1.1.1');

    if(window.StatusBar) {
      StatusBar.styleDefault();
      
      if (ionic.Platform.isAndroid()) {
    StatusBar.backgroundColorByHexString("#00833F");
                                      }
      }

    var notificationOpenedCallback = function(jsonData) {
    console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
    //$state.go('app.support');
  };

  window.plugins.OneSignal
    .startInit("70622dbd-f75f-4699-8aa8-0b93cdfa8a6e")
    .handleNotificationOpened(notificationOpenedCallback)
    .endInit();

  window.plugins.OneSignal.getIds(function(ids) {
    console.log("did",ids.userId);
    console.log(window.plugins.OneSignal);
  
});


  });
})


.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
     var isIOS = ionic.Platform.isIOS();
     var isAndroid = ionic.Platform.isAndroid();
    $ionicConfigProvider.tabs.position('bottom');

  if(isIOS){
    $ionicConfigProvider.navBar.alignTitle('center');
    $ionicConfigProvider.backButton.previousTitleText(false);
  }

  if(isAndroid){
   // $ionicConfigProvider.scrolling.jsScrolling(false);
    $ionicConfigProvider.navBar.alignTitle('center');
    $ionicConfigProvider.views.transition('platform');
    $ionicConfigProvider.backButton.icon('ion-ios-arrow-back');
    $ionicConfigProvider.backButton.text('');                  // default is 'Back'
    $ionicConfigProvider.backButton.previousTitleText(false);  // hides the 'Back' text
  }

  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

    .state('app.playlists', {
      url: '/playlists',
      views: {
        'menuContent': {
          templateUrl: 'templates/playlists.html',
          controller: 'PlaylistsCtrl'
        }
      }
    })

    .state('app.dashboard', {
    url: '/dashboard',
     views: {
      'menuContent@app': {
        templateUrl: 'templates/dashboard.html',
        controller: 'DashboardCtrl'
      }
    }     
  })

     
      .state('app.notifications', {
      url: '/notifications',
       views: {
      'menuContent': {
        templateUrl: 'templates/notifications.html',
        controller: 'NotificationsCtrl'
      }
    }     
  })

      .state('app.notification', {
        url: '/notifications/:itemId',
        views: {
          'menuContent': {
            templateUrl: 'templates/notification.html',
            controller: 'NotificationsCtrl'
          }
        }
      })

      


     .state('app.calculator', {
    url: '/calculator',
     views: {
      'menuContent': {
        templateUrl: 'templates/crane_calculator.html',
        controller: 'CalculatorCtrl'
      }
      },
      params: {
               formdata: null,
               species: null
             }    
  })
   
      .state('app.calcoutput', {
       url: '/calculator_output',
       views: {
      'menuContent': {
        templateUrl: 'templates/calc_output.html',
        controller: 'CalculatorOutputCtrl'        
           }       
         },
      params: {
               formdata: null,
               species: null
             } 
  })

      .state('app.calc_favorites', {
       url: '/calculator_favorites',
       views: {
      'menuContent': {
        templateUrl: 'templates/calc_favorites.html',
        controller: 'CalculatorFavoritesCtrl'        
           }       
         }
  })


     .state('app.aboutus', {
    url: '/aboutus',
     views: {
      'menuContent': {
        templateUrl: 'templates/aboutus.html',
        controller: 'AboutusCtrl'
      }
    }     
  })

      .state('landing', {
      url: '/landing',
      templateUrl: 'templates/landing.html',
      controller: 'LandingCtrl'
  })  

      .state('app.empsearch', {
        url: '/empsearch',       
        views: {
          'menuContent@app': {
            templateUrl: "templates/Empsearch_demo.html",
            controller: 'EmpSearchCtrl'
          }
        }            

      })

       // abstract state for a1
        .state('app.section_one_a1', {
          url: "/section_one/a1",
          abstract: true,
          views: {
            'menuContent': {
              templateUrl: "templates/ion-nav-view.html",
              controller: 'SectionOne_A1',
            }
          }
        })

          // abstract state for a1
        .state('app.section_one_a2', {
          url: "/section_one/a2",
          abstract: true,
          views: {
            'menuContent': {
              templateUrl: "templates/ion-nav-view.html",
              controller: 'SectionOne_A2',
            }
          }
        })


         .state('app.empsearch.empdetail', {
           cache: false,
           url: '/empdetail',
           views: {
              'menuContent@app':{
               templateUrl: "templates/employee_detail.html",
               controller: 'EmployeeDetailCtrl'
            }
           },         
           params: {selected_employee: null}
      })


          .state('app.section_one_a1.details', {
            url: '/timeoff',
             views: {
              'a-placeholder':{
              templateUrl: "templates/timeoff.html",
              controller: 'TimeOffCtrl'
            }
          },
           params:{id:null,
                   name:null,
                   selected_employee:null}
       })

         .state('app.section_one_a2.details', {
            url: '/directReports',
             views: {
              'a-placeholder':{
              templateUrl: "templates/directReports.html",
              controller: 'DirectReportsCtrl'
            }
          },
           params:{id:null,
                   name:null,
                   selected_employee:null}
       })


        .state('app.natureclock', {
        url: '/natureclock',
        views: {
          'menuContent': {
            templateUrl: 'templates/natureclock.html',
            controller: 'NatureClockCtrl'
          }
        }

      })

      
        .state('app.insects', {
        url: '/insects',
        views: {
          'menuContent': {
            templateUrl: 'templates/nature_insects.html',
            controller: 'NatureInsectCtrl'
          }
        },
          params: {
              naturedata: null
             }    

      })

       .state('app.natureinsectdetail', {
        url: "insectdetail/:year/:category/:insect",
        views: {
          'menuContent': {
            templateUrl: 'templates/nature_insects_detail.html',
            controller: 'NatureInsectDetailsCtrl'
          }
        }

      })
  
       .state('app.plants', {
        url: '/plants',
        views: {
          'menuContent': {
            templateUrl: 'templates/nature_plants.html',
            controller: 'NatureInsectCtrl'
          }
        },

        params: {
              naturedata: null
             }  

      })

       .state('app.natureplantdetail', {
        url: "plantsdetail/:year/:plant",
        views: {
          'menuContent': {
            templateUrl: 'templates/nature_plants_detail.html',
            controller: 'NaturePlantDetailsCtrl'
          }
        }

      })

      .state('app.naturefavorites', {
        url: "natureclock/favorites",
        views: {
          'menuContent': {
            templateUrl: 'templates/nature_favorites.html',
            controller: 'NatureFavoritesCtrl'
          }
        }

      })

      .state('app.client_lookup', {
        url: "/client_lookup",
        views: {
          'menuContent':{
            templateUrl: 'templates/Client_Lookup/client_home.html',
            controller: 'ClientLookupCtrl'
          }
        }
      })
      
      .state('app.client_search', {
        url: "/client_search",
        views: {
          'menuContent':{
            templateUrl: 'templates/Client_Lookup/client_search.html',
            controller: 'ClientSearchCtrl'
          }
        }
      })

      .state('app.client_search_result',{
          url: "/client_search_results",
          views: {
           'menuContent':{
             templateUrl: 'templates/Client_Lookup/client_search_results.html',
             controller: 'ClientSearchResultsCtrl'
           }
         }
      })


      .state('app.client_detail', {
        url: "clientdetail/:client",
        views: {
          'menuContent': {
            templateUrl: 'templates/Client_Lookup/client_detail.html',
            controller: 'ClientDetailsCtrl'
          }
        }

      })

        .state('app.client_filter', {
        url: "clientdetail/:client",
        views: {
          'menuContent': {
            templateUrl: 'templates/Client_Lookup/client_history_filter_modal2.html',
            controller: 'ClientHistoryFilterCtrl'
          }
        }

      })

       .state('app.support', {
        url: "/support",
        views: {
          'menuContent': {
            templateUrl: 'templates/support.html',
            controller: 'SupportCtrl'
          }
        }

      })

    //////////////////////////////////////////////////////////////////////////////////////

     .state('tab', {
      url: '/client_tabs',
      abstract: true,
      templateUrl: 'templates/Client_Lookup/client_detail_tabs.html',
    })
  .state('tab.basicinfo', {
    url: '/:client/basic_info',
    views: {
      'tab-basicinfo': {
        templateUrl: 'templates/Client_Lookup/Tabs/home.html',
        controller: 'ClientDetailsCtrl'
      }
    }
  })
  .state('tab.map', {
    url: '/:client/map',
    cache:false,
    views: {
      'tab-map': {
        templateUrl: 'templates/Client_Lookup/Tabs/client_map_display.html',
        controller: 'Client_MapsInfoCtrl'
      }
    }
  })
  .state('tab.history', {
    url: '/:client/history',
    views: {
      'tab-history': {
         templateUrl: 'templates/Client_Lookup/Tabs/client_history.html',
        controller: 'ClientHistoryCtrl'
      }
    }
  })



  
   


     .state('login', {
      url: '/login',
      templateUrl: 'templates/login.html',
      controller: 'LoginCtrl'
  });


  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/landing');
});