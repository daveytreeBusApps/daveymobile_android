   // var adfsUrl = "https://fsdev.davey.com/adfs/oauth2/authorize?response_type=code&client_id=2936c28b-2e1f-4955-a2ea-307b84c4deb2&resource=https://apptest.davey.com/testCors/&redirect_uri=https://apptest.davey.com/testCors/testOauth.html";
    
    var qa_adfsUrl = "https://fsdev.davey.com/adfs/oauth2/authorize?response_type=code&client_id=c35be863-f764-415e-8a71-9d36941fcb77&resource=https://wstest2.davey.com/daveyWS/&redirect_uri=https://wstest2.davey.com/daveyWS/frmRedir.aspx";
    var qa_tokenUrl = "https://fsdev.davey.com/adfs/oauth2/token";
    var qa_redirectURI = "https://wstest2.davey.com/daveyWS/frmRedir.aspx";
    var qa_client_id = "c35be863-f764-415e-8a71-9d36941fcb77";
    var dev_internal_rp_baseUrl  =  "https://kdevapp1.ad.davey-tree.com/daveyws/api/";
    var dev_rp_baseUrl = "https://wstest2.davey.com/daveyws/api/";
    

    var qa_internal_rp_baseUrl = "https://kqasapp1.ad.davey-tree.com/daveyws/api/";
    var qa_rp_baseUrl = "https://wstest3.davey.com/daveyws/api/"; 

    var prd_adfsUrl = "https://fs.davey.com/adfs/oauth2/authorize?response_type=code&client_id=94841d81-3191-41b9-9fbb-1335fe831db8&resource=https://ws4.davey.com/daveyWS/&redirect_uri=https://ws4.davey.com/daveyWS/frmRedir.aspx";
    var prd_tokenUrl = "https://fs.davey.com/adfs/oauth2/token";
    var prd_redirectURI = "https://ws4.davey.com/daveyWS/frmRedir.aspx";
    var prd_client_id = "94841d81-3191-41b9-9fbb-1335fe831db8"; 
    var prd_rp_baseUrl = "https://ws4.davey.com/daveyWS/api/";


    /*Common Params*/
    var authorization_grant = "authorization_code";
    var refresh_token = "refresh_token";  
    var options = {
      location: 'no',
      clearsessioncache: 'no',
      clearcache: 'no',
      toolbar: 'yes'
    };

    var emp_adfsurl = 'https://apptest.davey.com/testCors/EmployeeLookup/api/values?searchstring=';
    var natureclockurl = 'https://natureclock.daveyinstitute.com';
  
    

 
//Final Assignment
/*var currentSystem = "qa";
var adfsUrl =  qa_adfsUrl;
var tokenurl = qa_tokenUrl;
var client_id = qa_client_id;
var final_baseUrl = qa_rp_baseUrl;
var redirectURI = qa_redirectURI;*/


var currentSystem = "prd";
var adfsUrl =  prd_adfsUrl;
var tokenurl = prd_tokenUrl;
var client_id = prd_client_id;
var final_baseUrl = prd_rp_baseUrl;
//var final_baseUrl = dev_rp_baseUrl;
var redirectURI = prd_redirectURI;