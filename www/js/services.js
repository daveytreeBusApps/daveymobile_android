angular.module('starter.services', [])
  .service('AuthenticationService', function($q, $rootScope, $cordovaInAppBrowser, $http, $state, jwtHelper, $location, $cordovaNativeStorage) {
    function _getTokenFromStorage(token_key) {
      console.log("In AuthenticationService getTokenFromStorage method");
      console.log("Key", token_key);
      var access_token;
      return $cordovaNativeStorage.getItem(token_key)
        .catch(function(error) {
          console.log("GetItemError", error);
          return null;
        });
    }

    function _setTokenToStorage(token_key, value) {
      return $cordovaNativeStorage.setItem(token_key, value);
    }

    function _getNewAccessToken(token_key, token) {
      var accesstokenjson;
      var postData;
      var promise = $q.defer();
      var refreshCodeData = $.param({
        grant_type: refresh_token,
        client_id: client_id,
        redirect_uri: redirectURI,
        refresh_token: token
      });
      var authCodeData = $.param({
        grant_type: authorization_grant,
        client_id: client_id,
        redirect_uri: redirectURI,
        code: token
      });
      if (token_key == "authCode") {
        postData = authCodeData;
      } else
      if (token_key == "refresh_token") {
        postData = refreshCodeData;
      }
      //console.log("refreshcodeData", refreshCodeData);
      //console.log("authcodeData", authCodeData);
      var refreshedaccesstoken = $http.post(tokenurl, postData, {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded;'
        }
      });
      refreshedaccesstoken.success(function(data, status, headers, config) {
        //console.log("RefreshAccessToken Success", data);
        accesstokenjson = data;
        promise.resolve(accesstokenjson);
      });
      refreshedaccesstoken.error(function(data, status, headers, config) {
        //console.log("RefreshAccessToken Error");
        //console.log(data);
        promise.resolve(accesstokenjson);
        //return data;
      });

      return (promise.promise);
    }


    function _decodeAccessToken(access_token) {
      var tokenPayload = jwtHelper.decodeToken(access_token);
      var decoded = angular.toJson(tokenPayload);
      var json_output = $.parseJSON(decoded);
      return json_output;
    }

    function _removeTokenFromStorage(token_key) {
         return $cordovaNativeStorage.remove(token_key)
        .catch(function(error) {
          //console.log("Remove ItemError", error);
          return null;
        });
    }

    return {
      getTokenFromStorage: _getTokenFromStorage,
      setTokenToStorage: _setTokenToStorage,
      getNewAccessToken: _getNewAccessToken,
      decodeAccessToken: _decodeAccessToken,
      removeTokenFromStorage: _removeTokenFromStorage
    };



  })


  .service('LoginService', function($q, $rootScope, $cordovaInAppBrowser, $http, $state, jwtHelper, $location, $cordovaNativeStorage, AuthenticationService, NCAPIService) {
    function openBrowser() {
      var inAppBrowserRef;
      var promise1 = $q.defer();
      //console.log("openBrowser Function Called");
      //var ref = window.open(adfsUrl, '_blank','location=no,toolbar=yes');
      var target = "_blank";

      var options = "location=no,hidden=yes;clearsessioncache=no;clearcache=no";

      inAppBrowserRef = cordova.InAppBrowser.open(adfsUrl, target, options);

      inAppBrowserRef.addEventListener('loadstart', loadStartCallBack);

      inAppBrowserRef.addEventListener('loadstop', loadStopCallBack);

      inAppBrowserRef.addEventListener('loaderror', loadErrorCallBack);

      inAppBrowserRef.addEventListener('exit', exit);

      console.log("ref called");

      function loadStartCallBack(event) {

        //alert("loadstart called");
        console.log("loadstart called");

      }

      function loadStopCallBack(event) {
        // body...
        //alert("loadstop called");
      //  console.log("loadstop called");
      //  console.log(event);
        var str = event.url;
       // console.log("entered loadstop");
        if (inAppBrowserRef != undefined) {

          console.log("LoadStop Called");

          inAppBrowserRef.show();

          if (str.indexOf('?code') > 0) {
            var splits = str.split('=');
            var authCode = splits[1];
         //   console.log("Found AuthCode", authCode);
            //alert("Success");
            var token_key = "authCode";
            var cookie;
            AuthenticationService.getNewAccessToken(token_key, authCode)
              .then(function(accesstokenjson) {
                ///////////////////////////////////////////////////
                inAppBrowserRef.close();
               // NCAPIService.getandsetCookie(natureclockurl, cookie).then(function(data) {
              ///    console.log("Cookie Return NC", data);


                  promise1.resolve(accesstokenjson);
                });
             // });

           // console.log(promise1);


          }

          // inAppBrowserRef.close();
        }
      }

      function loadErrorCallBack(params) {

        var str = params.url;
        inAppBrowserRef.show();
          if (str.indexOf('?code') > 0) {
            var splits = str.split('=');
            var authCode = splits[1];
            //console.log("Found AuthCode", authCode);
            //alert("Success");
            var token_key = "authCode";
            var cookie;
            AuthenticationService.getNewAccessToken(token_key, authCode)
              .then(function(accesstokenjson) {
                ///////////////////////////////////////////////////
                inAppBrowserRef.close();
               // NCAPIService.getandsetCookie(natureclockurl, cookie).then(function(data) {
              ///    console.log("Cookie Return NC", data);


                  promise1.resolve(accesstokenjson);
                });
             // });

            //console.log(promise1);


          }else{
            
          
        alert('Sorry we cannot open that page. Message from the server is :' + params.message);
        //console.log('params message:', params.message);
        //console.log('Error PARAMS:', params);
        inAppBrowserRef.close();

        inAppBrowserRef = undefined;
        }
      }

      function exit(event) {

      }

      return (promise1.promise);


    }



    function getUserDetails(newaccesstoken) {
      var promise = $q.defer();
      //console.log("User Details Called", newaccesstoken);

      var access_token = newaccesstoken.access_token;
      var refresh_token = newaccesstoken.refresh_token;
      //console.log("New Access Token in User Details", access_token);
      //console.log("New Refresh Token in User Details", refresh_token);
      // isCertValid = KJUR.jws.JWS.verify(access_token, publicCertString, ['RS256']);
      // if (isCertValid) {
      var tokenoutput = AuthenticationService.decodeAccessToken(access_token);
      //console.log("Decoded Access Token", tokenoutput);
      var tokenjsonoutput = angular.toJson(tokenoutput);
      var tokenoutput2 = $.parseJSON(tokenjsonoutput);
      var userdetailtoken = JSON.stringify(tokenoutput2);
      userValid = true;
      //console.log("Certificate Valid tokenoutput2", tokenoutput2);
      /*Set the New Access Token from Storage*/
      token_key = 'access_token';
      AuthenticationService.setTokenToStorage(token_key, access_token)
        .then(function(access_token) {
          //console.log("New access Token Saved in Storage", access_token);
          token_key = 'userdetail';
          return AuthenticationService.setTokenToStorage(token_key, userdetailtoken);
        })
        .then(function(userdetailtoken) {
          //console.log("New User Details Saved in Storage", JSON.parse(userdetailtoken));
          token_key = 'refresh_token';
          if (refresh_token != null) {
            return AuthenticationService.setTokenToStorage(token_key, refresh_token);
          }
        })
        .then(function(refresh_token) {
          console.log("Refresh Token Saved in Storage", refresh_token);
          console.log("Final thing going to controller", tokenoutput2);
          promise.resolve(tokenoutput2);
        })
        .catch(function(error) {
          promise.reject(error);
        });

      // deferred.resolve(tokenoutput2);
      //console.log("Deferred is giving back this:", tokenoutput);
      // } else {
      //      console.log("Certificate Not Valid, Return NULL");
      // promise.resolve(null);
      //  }

      return (promise.promise);
    }
    ///////////////////////////////////////////////////////////////////////

    return {
      loginUser: function() {
        var deferred = $q.defer();
        var promise = deferred.promise;
        var isExpired;
        var userValid;
        var token_key;
        var newaccesstoken;
        var isCertValid;


        userValid = false;
        token_key = 'access_token';
        // var tokenoutput2 = null;

        return AuthenticationService.getTokenFromStorage(token_key)
          .then(function(access_token) {
            if (access_token != null) {
              //console.log("Got Access Token", access_token);
              isExpired = jwtHelper.isTokenExpired(access_token);
              console.log(isExpired);
              if (isExpired) {
                console.log("Access Token Expired");
                token_key = 'refresh_token';
               return AuthenticationService.removeTokenFromStorage('access_token').then(function(data){
                return AuthenticationService.getTokenFromStorage(token_key)
                  .then(function(ref_token) {
                    if (ref_token == null) {
                      return null;
                    } else {
                      //console.log("refresh token being passed", ref_token);
                      return AuthenticationService.getNewAccessToken(token_key, ref_token);
                    }
                  });
               });
             /*   return AuthenticationService.getTokenFromStorage(token_key)
                  .then(function(ref_token) {
                    if (ref_token == null) {
                      return null;
                    } else {
                      console.log("refresh token being passed", ref_token);
                      return AuthenticationService.getNewAccessToken(token_key, ref_token);
                    }
                  });*/
              }
            }
            return ({
              "access_token": access_token
            });
          })
          .then(function(newaccesstokenjson) {
            //console.log("newaccesstokenjson from first", newaccesstokenjson);
            if (newaccesstokenjson == null || newaccesstokenjson.access_token == null) {
              return (false);
            } else {
              return getUserDetails(newaccesstokenjson);
            }
          })
          .then(function(userValidjson) {
            if (!userValidjson) {
              return openBrowser()
                .then(function(accesstokenjson) {
                  return getUserDetails(accesstokenjson)
                    .catch(function(error) {
                      console.error('Error message 1');
                      return ($q.defer().reject());
                    });
                });
            } else {
              return (userValidjson);
            }
          })
          .catch(function(error) {
            console.error('There was a problem!');
          });
      }
    }
  })


    .service('empSalesOfficeService',function(){
    var empSalesOfficeList = [];

    return empSalesOfficeList;
  })

  .service('categoriesService', [
    '$http',
    'routesConfig',
    '$rootScope',
    'AuthenticationService',
    'empSalesOfficeService',
    '$q',
    function($http, routesConfig,$rootScope, AuthenticationService,empSalesOfficeService,$q) {
      'use strict';

      function _getCategories(demo_mode) {
        
        var categories;
        return $http.get(routesConfig.categories.all())
          .then(function(response) {
                 console.log("routesConfig");
                 categories = response;
               /////////////////////////////////////////////
               
                  var token_key = 'access_token';
                  
                return AuthenticationService.getTokenFromStorage(token_key);
                 }).then(function(access_token) {
                   console.log("Inside AuthenticationService");
                  $http.defaults.headers.common['Authorization'] = 'Bearer ' + access_token;
                 var employeeid = $rootScope.employeeid;
                   if (employeeid == '20055710' || employeeid == '20044002') {
     
                      employeeid = '898775';
       
                    }
                 
                  var url_getSalesOffice = final_baseUrl + 'Employees/getAllSalesOfficesForEmployee?employeeid=' + employeeid;

                   return $http.get(url_getSalesOffice);
                 }).then(function successCallback(salesOfficeresponse) {
                   // console.log("Get Sales Office Response", salesOfficeresponse.data);
                   // debugger;
                    empSalesOfficeService.empSalesOfficeList = salesOfficeresponse.data;
                   
                        if (salesOfficeresponse.data.length == 0) {
                          if($rootScope.employeeid !== "20044002" && $rootScope.employeeid !== "20055710"){

                         for (var item_counter = 0; item_counter < categories.data.rows.length; item_counter++) {
                          if (categories.data.rows[item_counter].id == 4) {
                            categories.data.rows.splice(item_counter, 1);
                          }
                         
                        }

                      }
                    }
                    else{
                      var selectedSalesOffice = empSalesOfficeService.empSalesOfficeList[0];
                       $rootScope.officeNo = selectedSalesOffice.officeno;
                       $rootScope.officeDesc = selectedSalesOffice.officeName;
                    }

                          //deferred.resolve(categories.data.rows);
                          return categories.data.rows;


                  }, function errorCallback(response) {
                   // alert("No Sales Office(s) Found For the Logged In User. Kindly, check with Davey Mobile App Team whether you are allowed to use this app");
                    for (var item_counter = 0; item_counter < response.data.rows.length; item_counter++) {
                          if (categories.data.rows[item_counter].id == 4) {
                            categories.data.rows.splice(item_counter, 1);
                          }
                         
                        }
                    $ionicLoading.hide();
                      return categories.data.rows;
                 /*   $ionicHistory.nextViewOptions({
                      historyRoot: true
                    });
                    $state.go('app.dashboard');*/
                  });

               
              
                  
      /////////////////////////////////////////////

          //  return response.data.rows;
          //return (deferred.promise);
         
          
      }


               

      function _getCategory(catId) {
        return _getCategories()
          .then(function(categories) {
            var categoriesLength = categories.length;

            if (categoriesLength && categoriesLength < 1) {
              return null;
            }

            for (var index = 0; index < categoriesLength; index++) {
              if (parseInt(categories[index].id, 10) === parseInt(catId, 10)) {
                return categories[index];
              }
            }

          });
      }

      return {
        getCategories: _getCategories,
        getCategory: _getCategory
      };
    }
  ])

  .service('notificationsService', [
    '$http',
    'routesConfig',
    function($http, routesConfig) {
      'use strict';

      function _getItems() {
        return $http.get(routesConfig.items.all())
          .then(function(response) {
            return response.data.rows;
          });
      }

      function _getItemById(itemId) {
        return _getItems()
          .then(function(items) {
            var itemsLength = items.length;

            if (itemsLength < 1) {
              return null;
            }

            for (var index = 0; index < itemsLength; index++) {
              if (parseInt(items[index].id, 10) === parseInt(itemId, 10)) {
                return items[index];
              }
            }

          });
      }

      return {
        getItems: _getItems,
        getItemById: _getItemById
      };
    }
  ])

  .service('FavoritesService', [
    '$http',
    '$localStorage',
    function($http, $localStorage) {
      'use strict';


      function _getAllItems(type) {
        var favorites = $localStorage.nc_favorites;
        if (favorites == null || favorites.length == 0) {
          favorites = $localStorage.nc_favorites = {
            plants: [],
            insects: []
          };
        }
        $localStorage.nc_favorites = favorites;
        console.log("Get All NC Favorite Items", $localStorage.nc_favorites);
        return favorites[type];
      }

      function _findItemByNameAndLocation(type, name, location) {
        var found = false;
        console.log("Find ITEMS", type);
        var allItems = _getAllItems(type);

        var itemsLength = allItems.length;

        if (itemsLength < 1) {
          return null;
        }


        for (var item_counter = 0, item_count = allItems.length; item_counter < item_count; item_counter++) {
          var item = allItems[item_counter];
          if (item.name == name && item.location == location) {
            found = true;
            break;
          }
        }

        return found;


      }

      function _addItems(type, name, location) {
        var added = false;
        var allItems = _getAllItems(type);
        var found = _findItemByNameAndLocation(type, name, location);
        if (!found) {
          allItems.push({ name: name, location: location });
          added = true;
        } else {
          added = true;
        }
        console.log("Newly ADDED ITEMS", allItems);

        return added; //if > -1 then added successfully.


      }

      function _removeItems(type, name, location) {
        var removed = false;
        var allItems = _getAllItems(type);

        for (var item_counter = 0, item_count = allItems.length; item_counter < item_count; item_counter++) {
          var item = allItems[item_counter];
          if (item.name == name && item.location == location) {
            allItems.splice(item_counter, 1);
            removed = true;
            break;
          }
        }

        console.log("Newly Removed ITEMS", allItems);

        return removed; //if > -1 then added successfully.

      }

      return {
        plants: {
          getAllFavoriteItems: function() {
            return _getAllItems('plants');
          },
          addFavoriteItems: function(name, location) {
            return _addItems('plants', name, location);
          },
          removeFavoriteItems: function(name, location) {

            return _removeItems('plants', name, location);
          },
          findFavItemByNameAndLocation: function(name, location) {
            return _findItemByNameAndLocation('plants', name, location)
          }
        },
        insects: {
          getAllFavoriteItems: function() {
            return _getAllItems('insects');
          },
          findFavItemByNameAndLocation: function(name, location) {
            return _findItemByNameAndLocation('insects', name, location);
          },
          addFavoriteItems: function(name, location) {
            return _addItems('insects', name, location);
          },
          removeFavoriteItems: function(name, location) {
            return _removeItems('insects', name, location);
          }
        }
      };

    }
  ])
   

  .service('CustomersFavoritesService', [
    '$http',
    '$localStorage',
    function($http, $localStorage) {
      'use strict';


      function _getAllItems(type) {
        var favorites = $localStorage.client_favorites;
        if (favorites == null || favorites.length == 0) {
          favorites = $localStorage.client_favorites = {
            clients: [],
            prospects: []
          };
        }
        $localStorage.client_favorites = favorites;
        console.log("Get All Client Favorite Items",  $localStorage.client_favorites);
        return favorites[type];
      }

      function _findItemByNameAndType(type, name) {
        var found = false;
        console.log("Find ITEMS", type);
        var allItems = _getAllItems(type);

        var itemsLength = allItems.length;

        if (itemsLength < 1) {
          return null;
        }


        for (var item_counter = 0, item_count = allItems.length; item_counter < item_count; item_counter++) {
          var item = allItems[item_counter];
          if (item.name == name) {
            found = true;
            break;
          }
        }

        return found;


      }

      function _addItems(type, name) {
        var added = false;
        var allItems = _getAllItems(type);
        var found = _findItemByNameAndType(type, name);
        if (!found) {
          allItems.push({ name: name, type: type });
          added = true;
        } else {
          added = true;
        }
        console.log("Newly ADDED ITEMS", allItems);

        return added; //if > -1 then added successfully.


      }

      function _removeItems(type, name) {
        var removed = false;
        var allItems = _getAllItems(type);

        for (var item_counter = 0, item_count = allItems.length; item_counter < item_count; item_counter++) {
          var item = allItems[item_counter];
          if (item.name == name) {
            allItems.splice(item_counter, 1);
            removed = true;
            break;
          }
        }

        console.log("Newly Removed ITEMS", allItems);

        return removed; //if > -1 then added successfully.

      }

      return {
        clients: {
          getAllFavoriteItems: function() {
            return _getAllItems('clients');
          },
          addFavoriteItems: function(name) {
            return _addItems('clients', name);
          },
          removeFavoriteItems: function(name) {

            return _removeItems('clients', name);
          },
          findItemByNameAndType: function(name) {
            return _findItemByNameAndType('clients', name)
          }
        },
        prospects: {
          getAllFavoriteItems: function() {
            return _getAllItems('prospects');
          },
          findItemByNameAndType: function(name) {
            return _findItemByNameAndType('prospects', name);
          },
          addFavoriteItems: function(name) {
            return _addItems('prospects', name);
          },
          removeFavoriteItems: function(name) {
            return _removeItems('prospects', name);
          }
        }
      };

    }
  ])



  .service('NCAPIService', function($http, $q, $localStorage, $timeout,AuthenticationService) {
    'use strict';

    function _getandsetCookie(url, cookie) {
      var promise1 = $q.defer();
      var inAppBrowserRef;
      // var url = 'https://natureclock.daveyinstitute.com/natureclocksvc.cfc?method=getListOfLocation';
      var target = "_blank";

      var options = "location=yes,hidden=yes,clearcache=no,clearsessioncache=no";



      /*  if(cookie != null){
          window.cookieEmperor.setCookie(url, cookie.name, cookie.val,
            function() {
             console.log('A cookie has been set');
             },
           function(error) {
             console.log('Error setting cookie: '+error);
          });
        }*/

      inAppBrowserRef = cordova.InAppBrowser.open(url, "_blank", options);

      inAppBrowserRef.addEventListener('loadstart', loadStartCallBack);

      inAppBrowserRef.addEventListener('loadstop', loadStopCallBack);

      inAppBrowserRef.addEventListener('loaderror', loadErrorCallBack);


      function loadStartCallBack(params) {
        //    alert("LoadStart");
        console.log("loadstart", params);

      }

      function loadStopCallBack(params) {
        //alert("LoadStop");
        if (inAppBrowserRef != undefined) {
            inAppBrowserRef.show();
          console.log("Loadstop Params", params);
          if (params.url == 'https://natureclock.daveyinstitute.com/') {
            promise1.resolve();
            inAppBrowserRef.close();
          }


          /*         window.cookieEmperor.getCookie('https://natureclock.daveyinstitute.com/natureclocksvc.cfc?method=getListOfLocation', 'mellon-natureclock.daveyinstitute.com_authenication', function(data) {
                   console.log("cookie value", data.cookieValue);
                   var cookie_name = 'mellon-natureclock.daveyinstitute.com_authenication';
                      if (data.cookieValue != 'cookietest') {
                          $localStorage.nc_cookie = {};
                          var nc_cookie = {name: cookie_name,
                                           val: data.cookieValue}; 
                          $localStorage.nc_cookie = nc_cookie;
                          console.log("Cookie Saved", $localStorage.nc_cookie);
                          promise1.resolve(data.cookieValue);
                      }
                  }, function(error) {
                   if (error) {
                  console.log('cookie error: ' + error);
                  }
               });*/


        }

      }

      function loadErrorCallBack(params) {

        var scriptErrorMesssage =
          "alert('Sorry we cannot open that page. Message from the server is : " +
          params.message + "');"

        /*inAppBrowserRef.executeScript({
            code: scriptErrorMesssage
            }, executeScriptCallBack);*/

        inAppBrowserRef.close();

        inAppBrowserRef = undefined;

      }

      return (promise1.promise);

    }


    function _xml2JSON(xml) {
      var parsedXml = parseXml(xml);
      var JsonOutput = xml2json(parsedXml, '\t');

      function parseXml(xml) {
        var dom = null;
        if (window.DOMParser) {
          try {
            dom = (new DOMParser()).parseFromString(xml, "text/xml");
          } catch (e) { dom = null; }
        } else if (window.ActiveXObject) {
          try {
            dom = new ActiveXObject('Microsoft.XMLDOM');
            dom.async = false;
            if (!dom.loadXML(xml)) // parse error ..
              window.alert(dom.parseError.reason + dom.parseError.srcText);
          } catch (e) { dom = null; }
        } else
          alert("oops");
        return dom;
      }

      return JsonOutput;
    }

    function _getURL(demo_mode, location) {
      var naturedata_url;
      var naturelocation_url;
      var output;
      if (demo_mode) {
        output = { location: location, naturedata_url: "feeds/natureoutput.json", naturelocation_url: "feeds/locations.json" };



      } else {
        output = {
          location: location,
          naturedata_url: "https://natureclock.daveyinstitute.com/natureclocksvc.cfc?method=getNatureClockOutput&locationcode=" + location,
          naturelocation_url: "https://natureclock.daveyinstitute.com/natureclocksvc.cfc?method=getListOfLocation"
        };
      }

      return output;
    }

    function _getAllDataFromAPI(url, currentLocation) {
     return AuthenticationService.getTokenFromStorage('access_token').then(function(access_token) { 
        $http.defaults.headers.common['Authorization'] = 'Bearer ' + access_token; 
      return $http.get(url).then(function successCallback(response) {
          var nature_insect_response = response.data;

          var x2js = new X2JS({ attributePrefix: "none", stripWhitespaces: true });
          var JsonOutput1 = x2js.xml_str2json(response.data);
          var str_output = JSON.stringify(JsonOutput1);
          var replaced = str_output.replace(/none/g, '');
          var JsonOutput = JSON.parse(replaced);
          console.log("JSON Output", JsonOutput);

          var output = {
            currentLocation: currentLocation,
            currentYear: JsonOutput.NatureClockOutput.Year,
            insects: JsonOutput.NatureClockOutput.Insects, //parsedinsects, 
            plants: JsonOutput.NatureClockOutput.Plants //parsedplants
          };

          return output;
        },
        function errorCallback(response) {
          alert("Unable to Load Nature Clock Data");
          return '';

        });
      });

    }

    function _getAllLocationsFromAPI(url) {

    return AuthenticationService.getTokenFromStorage('access_token').then(function(access_token) { 
       $http.defaults.headers.common['Authorization'] = 'Bearer ' + access_token; 
      return $http.get(url).then(function successCallback(response) {
          var states = [];
          console.log("xmlresponse", response.data);
          var x2js = new X2JS();
          var JsonOutput = x2js.xml_str2json(response.data);
          console.log("JSONOUTPUT", JsonOutput);
          JsonOutput.NatureClockWeatherLocations.Location.map(function(location) {

            var existingState = states.find(function(state) {
              return state.StateAbbreviation == location.StateAbbreviation;
            });

            if (!existingState) {
              existingState = {
                StateAbbreviation: location.StateAbbreviation,
                cities: []
              };

              states.push(existingState);
            }
            var existingCity = existingState.cities.find(function(city) {

              return city.CityName == location.CityName;
            });

            if (!existingCity) {
              existingState.cities.push({
                cityName: location.CityName,
                locationCode: location.LocationCode
              });
            }
          });


          console.log("Final States Array", states);
          console.log("Final States Array length", states.length);

          return states;

        },

        function errorCallback(response) {
          alert("Unable to Load Species");
          return '';

        }
      );

    });
  }




    function _getInsectFromNameandLocation(name, url, location) {
      return _getAllDataFromAPI(url, location).then(function(data) {

        var insects = data.insects.Category;
        var currentCategory;
        var currentYear = data.currentYear;
        var currentInsects = [];
        var getOut = false;
        for (var categoryCounter = 0, categoryCount = insects.length; categoryCounter < categoryCount; categoryCounter++) {
          if (getOut) {
            break;
          }
          currentCategory = insects[categoryCounter].Name; //  Reference for convenience
          var insects_list = insects[categoryCounter].Insect;

          var isInsectArray = angular.isArray(insects_list);
          if (isInsectArray) {
            currentInsects = insects_list;
          } else {
            currentInsects.push(insects_list);
          }

          for (var insectCounter = 0, insectCount = currentInsects.length; insectCounter < insectCount; insectCounter++) {
            if (name == currentInsects[insectCounter].Name) {
              var insectOutput = { category: currentCategory, insect: currentInsects[insectCounter], currentYear: currentYear };
              return insectOutput;
              getOut = true;
              break;
            }
          }
        }

      })

    }

    function _getPlantFromNameandLocation(name, url, location) {
      return _getAllDataFromAPI(url, location).then(function(data) {

        var currentPlants = data.plants.Plant;
        var currentYear = data.currentYear;

        for (var plantCounter = 0, plantCount = currentPlants.length; plantCounter < plantCount; plantCounter++) {
          if (name == currentPlants[plantCounter].Name) {
            var plantOutput = { plant: currentPlants[plantCounter], currentYear: currentYear };
            return plantOutput;
            break;
          }
        }

      })

    }


    return {
      getandsetCookie: _getandsetCookie,
      getURL: _getURL,
      getAllDataFromAPI: _getAllDataFromAPI,
      getInsectFromNameandLocation: _getInsectFromNameandLocation,
      getPlantFromNameandLocation: _getPlantFromNameandLocation,
      getAllLocationsFromAPI: _getAllLocationsFromAPI
    };

  })

  .service('CraneAPIService', [
    '$http',
    '$localStorage',
    function($http, $localStorage) {
      'use strict';

      function _getAllFavItems() {
        var favorites = $localStorage.crane_favorites;
        if (favorites == null) {
          favorites = $localStorage.crane_favorites = [];

        };

        $localStorage.crane_favorites = favorites;
        console.log("Get All Items", favorites);
        return favorites;
      }


      function _addFavItems(species, description, formdata) {
        var added = false;
        var currentRecord = _findItemByName(species, description);
        var allItems = _getAllFavItems();
        if (currentRecord.name) {
          var d = new Date();
          var curr_date = d.getDate();
          var curr_month = d.getMonth();

          for (var item_counter = 0; item_counter < allItems.length; item_counter++) {
            if (allItems[item_counter].name == currentRecord.name) {
              allItems[item_counter].values.push({ description: description, formdata: formdata, date: curr_date, month: curr_month });
              added = true;
            }

          }


        } else {
          alert("Unable to add it to BookMarks");
        }
        console.log("Newly ADDED Crane Calculation", $localStorage.crane_favorites);

        return added; //if > -1 then added successfully.


      }

      function _findItemByName(species, description) {
        var found = false;
        var newItem = {};
        var allItems = _getAllFavItems();

        for (var item_counter = 0, item_count = allItems.length; item_counter < item_count; item_counter++) {
          newItem = allItems[item_counter];
          if (newItem.name == species.name) {
            found = true;
            break;
          }
        }

        if (!found) {
          newItem = {
            name: species.name,
            wt: species.wt,
            values: []
          };
          allItems.push(newItem);

        }

        return newItem;

      }

      return {
        getAllFavItems: _getAllFavItems,
        findItemByName: _findItemByName,
        addFavItems: _addFavItems
      };

    }


  ])

  .service('clientService', function(){
    var result = {client: null};
    return result; 
  })

  .service('clientResultService', function(){
    var client_search_results = {clientList: null};
    return client_search_results; 
  })

  .service('yearRangeService', function(){
   var yearRange = {fromYear: null,
                    toYear: null};
    return yearRange; 
  })



.service('checkUserIDService', function($rootScope){
    var isAllowedAccess = false;
    if($rootScope.employeeid == '20055710' || $rootScope.employeeid == '20044002'){
      isAllowedAccess = true;
    }
    
      return isAllowedAccess;
})
