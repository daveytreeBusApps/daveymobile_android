angular.module('starter.controllers', [])

  .controller('AppCtrl', function($scope, $rootScope, $ionicModal, $timeout, LoginService, $ionicPopup) {
    $scope.username = $rootScope.username;
    $scope.profilePictureUrl = "img/ic_launcher.png";
    $scope.title = 'Employee';
    $scope.appVersion = "1.0.1(0.0.6)";

  })

  .controller('LoginCtrl', function($scope, $timeout, $rootScope, LoginService, $ionicPopup, $state, $location, $timeout, $ionicLoading) {
    $scope.data = {};

    $ionicLoading.show({
      template: '<ion-spinner icon="bubbles"></ion-spinner><p>Logging In...</p>'
    });
    LoginService.loginUser().then(function(data) {
      console.log("Login Data", data);
      if (data == undefined || data == null) {
        var alertPopup = $ionicPopup.alert({
          title: 'Login failed!',
          template: 'No User Details Found'
        });
        alertPopup.then(function(res) {
          console.log('Exiting App');
          ionic.Platform.exitApp();
        });
      } else {
        $rootScope.username = data.given_name + ' ' + data.family_name;
        $rootScope.useremail = data.upn;
        $rootScope.costcenter = data.dvyDivision;
        $rootScope.employeeid = data.dvyEmployeeID;
      
        $location.path('app/dashboard');
        window.ga.setUserId($rootScope.useremail);
        window.ga.trackEvent('UserLogin', $rootScope.useremail, 'Login');
        if($rootScope.useremail == "daveymobile.user@davey.com"){
          $rootScope.demo_mode = 'X';
        }
      }
      $ionicLoading.hide();
    }, (function(error) {
      //$location.path('app/login');
      var alertPopup = $ionicPopup.alert({
        title: 'Login failed!',
        template: 'Please check your credentials!'

      });
      $ionicLoading.hide();

    }));

    //}, 2000); 

  })

  .controller('LandingCtrl', function($scope, $rootScope, $location, $ionicHistory, $state) {
    $scope.goto = function(mode) {
          $state.go('login');
    }
  })
  .controller('DashboardCtrl', function($scope, categoriesService, $cordovaInAppBrowser, $ionicHistory, $state,
    $window, $rootScope,$ionicLoading) {
    window.ga.setUserId($rootScope.useremail, function(result) {
      console.log("setGoogleAnalyticsUserID", result);
    });
    window.ga.trackView('Dashboard');
    $ionicLoading.show({
        template: '<ion-spinner></ion-spinner> <br/> Fetching Tiles'
     });
    $scope.$on("$ionicView.enter", function() {
     
      $ionicHistory.clearHistory();

    });


    $scope.onDropComplete = function(index, obj, evt) {
      alert("Drop onDropComplete");
      var otherObj = $scope.draggableObjects[index];
      var otherIndex = $scope.draggableObjects.indexOf(obj);
      $scope.draggableObjects[index] = obj;
      $scope.draggableObjects[otherIndex] = otherObj;
    }

    var gridClass = "col col-50";
    $scope.calculateDimensions = function(gesture) {

      $scope.dev_width = $window.innerWidth;
      $scope.dev_height = $window.innerHeight;
    }

    angular.element($window).bind('resize', function() {
      $scope.$apply(function() {
        $scope.calculateDimensions();
      })
    });

    $scope.calculateDimensions();
   
    console.log($scope.dev_width);
    categoriesService.getCategories($rootScope.demo_mode).then(setCategories);


    function setCategories(categories) {
      console.log("Categories Found", categories);

      $scope.categories = categories;
      $ionicLoading.hide();
    }

    $scope.openService = function(id, categoryurl, categoryTitle) {
      if (categoryurl.indexOf("http") !== -1) {
        var options1 = {
          hidden: 'no',
          location: 'no',
          clearcache: 'no',
          clearsessioncache: 'no',
          toolbar: 'yes'
        };
       // console.log(categoryurl);
        window.ga.trackView(categoryTitle);
        openUrl(categoryurl, true);

        function openUrl(url, readerMode) {
          SafariViewController.isAvailable(function(available) {
            if (available) {
              SafariViewController.show({
                  url: url,
                  hidden: false, // default false. You can use this to load cookies etc in the background (see issue #1 for details).
                  animated: false, // default true, note that 'hide' will reuse this preference (the 'Done' button will always animate though)
                  transition: 'curl', // (this only works in iOS 9.1/9.2 and lower) unless animated is false you can choose from: curl, flip, fade, slide (default)
                  enterReaderModeIfAvailable: readerMode, // default false
                  tintColor: "#00ffff", // default is ios blue
                  toolbarColor: "#00843D", // on iOS 10+ you can change the background color as well
                  controlTintColor: "#ffffff" // on iOS 10+ you can override the default tintColor
                },
                // this success handler will be invoked for the lifecycle events 'opened', 'loaded' and 'closed'
                function(result) {
                  if (result.event === 'opened') {
                    console.log('opened');
                  } else if (result.event === 'loaded') {
                    console.log('loaded');
                  } else if (result.event === 'closed') {
                    console.log('closed');
                  }
                },
                function(msg) {
                  console.log("KO: " + msg);
                })
            } else {
              // potentially powered by InAppBrowser because that (currently) clobbers window.open
              var InAppBrowserRef = cordova.InAppBrowser.open(url, '_blank', 'location=no;hidden=yes;clearsessioncache=no;clearcache=no');
              InAppBrowser.show();
            }
          })
        }

        function dismissSafari() {
          SafariViewController.hide()
        }
      } else {

        $state.go(categoryurl);


      }
    }
  })


  .controller('CalculatorCtrl', function($scope, $ionicModal, $state, $rootScope, $http, $ionicScrollDelegate, $stateParams, $localStorage, $ionicPlatform) {
    window.ga.trackView('Crane Calculator');

    var position = $ionicScrollDelegate.getScrollPosition();
    $scope.$on("$ionicView.enter", function() {
      var position2 = $ionicScrollDelegate.getScrollPosition();
      if (!position2.top == 0) {
        $ionicScrollDelegate.scrollTo(0, 0, true);
      }

    });

    // run this function when either hard or soft back button is pressed
    var doCustomBack = function() {
      $state.go('app.dashboard');
    };

    // override soft back
    // framework calls $rootScope.$ionicGoBack when soft back button is pressed
    var oldSoftBack = $rootScope.$ionicGoBack;
    $rootScope.$ionicGoBack = function() {
      doCustomBack();
    };
    var deregisterSoftBack = function() {
      $rootScope.$ionicGoBack = oldSoftBack;
    };

    // override hard back
    // registerBackButtonAction() returns a function which can be used to deregister it
    var deregisterHardBack = $ionicPlatform.registerBackButtonAction(
      doCustomBack, 101
    );

    // cancel custom back behaviour
    $scope.$on('$destroy', function() {
      deregisterHardBack();
      deregisterSoftBack();
    });


    $scope.showDisclaimerModal = function() {
      return $ionicModal.fromTemplateUrl('templates/crane_disclaimer.html', {
        scope: $scope,
        animation: "animated bounceInDown",
        backdropClickToClose: !1,
        hardwareBackButtonClose: !1
      }).then(function(modal) {
        $scope.disclaimer_modal = modal;
      });
    }

    $scope.openModal = function() {
      console.log("Disclaimer Modal", $scope.disclaimer_modal);
      $scope.disclaimer_modal.show();
    };

    $scope.openDisclaimerModal = function() {
      $scope.showDisclaimerModal().then($scope.openModal);
    }



    $scope.disclaimerShown = $localStorage.crane_disclaimerShown;
    if (!$scope.disclaimerShown) {
      //$scope.showDisclaimerModal().then($scope.openDisclaimerModal);
      $scope.openDisclaimerModal();
    }

    $scope.disclaimer_Accept = function() {
      $localStorage.crane_disclaimerShown = true;
      $scope.fab = false;
      $scope.disclaimer_modal.hide();

    }




    $ionicModal.fromTemplateUrl('templates/search_species.html', {
      scope: $scope,
      animation: 'slide-in-up',
    }).then(function(modal) {
      $scope.modal = modal;
    });

    $scope.openspeciesModal = function() {
      $http({
        method: 'GET',
        url: 'feeds/species.json'
      }).then(function successCallback(response) {
        $scope.species = response.data;
        $scope.totalCount = $scope.species.length;
      }, function errorCallback(response) {
        alert("Unable to Load Species");

      });
      $scope.modal.show().then(function() {
        $scope.fadeBuildForm = true;
      })
    }


    $scope.calculate = function(formdata) {
      console.log("formdata", formdata);
      $scope.isError = false;

      if (formdata.calc.d_large == null || formdata.calc.d_middle == null || formdata.calc.d_small == null || formdata.calc.length == null) {
        $scope.isError = true;
        $scope.form_error_msg = 'Please fill in all fields';
      }


      if (!$scope.isError) {
        $state.go('app.calcoutput', {
          "formdata": formdata,
          "species": $scope.currentspecies
        });
      }

    }
    $scope.currentspecies = {};


    console.log("StateParams", $stateParams);
    if ($stateParams.species) {
      $scope.formData = {};
      $scope.currentspecies.name = $stateParams.species.name;
      $scope.currentspecies.wt = $stateParams.species.wt;
      $scope.isCurrentSpecies = true;
      $scope.formData.calc = $stateParams.formdata.calc;
      console.log("Scope Formdata", $scope.formData);
    }

    $scope.setspecies = function(species) {
      console.log(species);
      $scope.currentspecies = species;
      $scope.isCurrentSpecies = true;
      $scope.modal.hide();
    }

    $scope.clearSearch = function() {
      $scope.search = '';
    };

    $scope.scrollTop = function() {
      $ionicScrollDelegate.scrollTop();
    };

    $scope.fab = false;

    $scope.toggleFab = function(fab) {
      $scope.fab = !$scope.fab;
    };

    $scope.openFavorites = function() {
      $scope.fab = false;
      $state.go('app.calc_favorites');
    };

  })


  .controller('CalculatorFavoritesCtrl', function($scope, CraneAPIService, $localStorage, $state, $ionicPopup) {
    window.ga.trackView('Crane Calculator Favorites');
    $scope.craneFavorites = CraneAPIService.getAllFavItems();
    console.log("Crane Favorites", $scope.craneFavorites);


    $scope.deleteAllFavorites = function() {
      $localStorage.crane_favorites = [];
    }

    $scope.setFavSpecies = function(species, value) {
      console.log("Sending to Calculator", value.formdata);
      console.log("Sending to Calculator2", species);
      $state.go('app.calculator', {
        "formdata": value.formdata,
        "species": species
      });
    }

    $scope.data = {
      showDelete: false
    };

    $scope.onItemDelete = function(description, name) {
      var exit = false;
      var confirmPopup = $ionicPopup.confirm({
        title: 'Deletion Confirmation',
        template: 'Do you want to delete the current Item?'
      });

      confirmPopup.then(function(res) {
        if (res) {
          console.log('Sure!');
          for (var item_counter = 0; item_counter < $localStorage.crane_favorites.length; item_counter++) {
            if (name == $localStorage.crane_favorites[item_counter].name) {
              var currentspecies_values = $localStorage.crane_favorites[item_counter].values;

              if (exit) {
                break;
              }
              for (var value_counter = 0; value_counter < currentspecies_values.length; value_counter++) {
                if (currentspecies_values[value_counter].description == description) {
                  currentspecies_values.splice(currentspecies_values[value_counter], 1);
                  if (currentspecies_values.length == 0) {
                    $localStorage.crane_favorites.splice($localStorage.crane_favorites[item_counter], 1);
                  }
                  exit = true;
                  break;
                }
              }
            }
          }

        } else {
          console.log('Not sure!');
        }
      });

    }


  })

  .controller('CalculatorOutputCtrl', function($scope, $rootScope, $state, $ionicPopup, $stateParams, CraneAPIService, $localStorage) {
    var dlarge_calc = math.pow($stateParams.formdata.calc.d_large, 2);
    var dmiddle_calc = math.pow($stateParams.formdata.calc.d_middle, 2);
    var dsmall_calc = math.pow($stateParams.formdata.calc.d_small, 2);
    var length = $stateParams.formdata.calc.length;

    $scope.volume = math.round((0.000909 * (dlarge_calc + (4 * dmiddle_calc) + dsmall_calc) * length), 2);
    $scope.weight = math.round(($scope.volume * $stateParams.species.wt), 2);

    $scope.calcAgain = function() {
      console.log("stateParams in Output", $stateParams);
      $state.go('app.calculator', {
        "formdata": $stateParams.formdata,
        "species": $stateParams.species
      });
    }

    $scope.save = function() {
      $scope.data = {};
      var saved = false;
      var myPopup = $ionicPopup.show({
        template: '<input type = "text" ng-model = "data.model">',
        title: 'Enter Description',
        // subTitle: 'Subtitle',
        scope: $scope,

        buttons: [{
            text: 'Cancel',
            onTap: function(e) { return true; }
          },
          {
            text: 'Save',
            type: 'button-positive',
            onTap: function(e) {
              if (!$scope.data.model) {
                //don't allow the user to close unless he enters model...
                e.preventDefault();
              } else {
                saved = true;
                return $scope.data.model;
              }
            }
          }
        ]
      });

      myPopup.then(function(description) {
        if (description && saved) {
          var added = CraneAPIService.addFavItems($stateParams.species, description, $stateParams.formdata);
          if (added) {
            window.plugins.toast.showWithOptions({
              message: "Added To Favorites Successfully",
              duration: "short", // which is 2000 ms. "long" is 4000. Or specify the nr of ms yourself.
              position: "center",
              addPixelsY: -40 // added a negative value to move it up a bit (default 0)
            });
          }

          console.log("crane Favorites", $localStorage.crane_favorites);
        }
      });
    }
  })


  .controller('NotificationsCtrl', function($scope, $stateParams, $ionicModal, notificationsService) {
    window.ga.trackView('Notifications');


    notificationsService.getItems().then(setNotificationitems);

    function setNotificationitems(items) {
      console.log("The Notifications items are ", items);
      $scope.items = items;
    }

    console.log("StateParams", $stateParams);
    if ($stateParams && $stateParams.hasOwnProperty('itemId')) {

      notificationsService.getItemById(parseInt($stateParams.itemId, 10))
        .then(setItem);

    }

    function setItem(item) {
      $scope.item = item;
      $scope.share = {
        'networks': ['facebook', 'twitter', 'whatsapp', 'anywhere', 'sms', 'email'],
        'message': item.title,
        'subject': item.category.title,
        'file': item.img,
        'link': item.link,
        'toArr': ['info@surfit.mobi'],
        'bccArr': [],
        'ccArr': [],
        'phone': '098765432'
      }
    }

  })

  .controller('myTeamCtrl', function($scope) {
    $scope.showFab = false;
  })

  .controller('EmpSearchCtrl', function($scope, $http, $rootScope, $ionicLoading, $ionicModal, $ionicPopup, $state, AuthenticationService, $location) {

    window.ga.trackView('Davey Employee Lookup');
    $ionicLoading.show({
      content: 'Loading',
      animation: 'fade-in',
      showBackdrop: true,
      maxWidth: 200,
      showDelay: 0
    });


    $scope.toggleGroup = function(group) {
      group.show = !group.show;
    };
    $scope.isGroupShown = function(group) {
      return group.show;
    };


    var token_key = 'access_token';

    $scope.clearSearch = function() {
      this.searchval = null;
      $scope.globalemployees = null;
    }


    if ($rootScope.demo_mode) {

        $http({
          method: 'GET',
          url: "feeds/myRecord.json"
        }).then(function successCallback(response) {
          $scope.mytitle = response.data[0].title;
          $scope.mylastName = response.data[0].lastName;
          $scope.myfirstName = response.data[0].firstName;
          $scope.myBusinessPhone = response.data[0].BusinessPhone;
          $scope.myMobilePhone = response.data[0].MobilePhone;
          $scope.myemail = response.data[0].email;
          $scope.myManagerName = response.data[0].managerInfo.fullName;
          $scope.myManagerEmail = response.data[0].managerInfo.Email;
          $scope.myManagerBusinessPhone = response.data[0].managerInfo.BusinessPhone;
          $scope.myManagerMobilePhone = response.data[0].managerInfo.MobilePhone;
          $scope.myDepartment = response.data[0].department;
          $scope.myOffice = response.data[0].office;
          $scope.groups = [];
          for (var i = 0; i < 1; i++) {
            $scope.groups[i] = {
              name: $scope.myManagerName,
              items: [],
              show: false
            };
          }


        });
     
      $http({
        method: 'GET',
        url: 'feeds/employees.json'
      }).then(function successCallback(response) {
        console.log(response);
        $ionicLoading.hide();
        $scope.myteamemployees = response.data;
        console.log("My Team Employees", $scope.myteamemployees);
      }, function errorCallback(response) {
        $ionicLoading.hide();
        alert("error");

      });



    } else {

      //////////////////////////////////////////////////
      //$rootScope.employeeid = '20027482';
      // $rootScope.costcenter = '676607';
      //////////////////////////////////////////////////
      var employeeid = $rootScope.employeeid;
     // employeeid = '20044002';
      AuthenticationService.getTokenFromStorage(token_key).then(function(access_token) {
        $http.defaults.headers.common['Authorization'] = 'Bearer ' + access_token;

        //final_baseUrl = dev_internal_rp_baseUrl;
        //var url = final_baseUrl + "Employees/GetEmployees?searchString=" + $rootScope.employeeid + "&showdirectReports=X";
        //var url = final_baseUrl + "Employees/GetEmployees?searchString=" + employeeid + "&showdirectReports=X";
        var url = final_baseUrl + "Employees/SearchEmployees?employeeid=" + employeeid;
        $http({
          method: 'GET',
          url: url
        }).then(function successCallback(response) {
          console.log("GetEmployees Response", response.data);
          $scope.mytitle = response.data[0].title;
          $scope.mylastName = response.data[0].lastName;
          $scope.myfirstName = response.data[0].firstName;
          $scope.myBusinessPhone = response.data[0].BusinessPhone;
          $scope.myMobilePhone = response.data[0].MobilePhone;
          $scope.myemail = response.data[0].email;
          $scope.myManagerName = response.data[0].managerInfo.fullName;
          $scope.myManagerEmail = response.data[0].managerInfo.Email;
          $scope.myManagerBusinessPhone = response.data[0].managerInfo.BusinessPhone;
          $scope.myManagerMobilePhone = response.data[0].managerInfo.MobilePhone;
          $scope.myDepartment = response.data[0].department;
          $scope.myOffice = response.data[0].office;
          $scope.groups = [];
          for (var i = 0; i < 1; i++) {
            $scope.groups[i] = {
              name: $scope.myManagerName,
              items: [],
              show: false
            };
          }

          //Employees assigned to one or more cost centers

          var myteamurl = final_baseUrl + "Employees/SearchEmployees?costcenter=" + $rootScope.costcenter;
          $http.get(myteamurl).then(function(data) {
            var myteamemployees = data.data;

            if (response.data[0].Directreports != null) {
              angular.forEach(response.data[0].Directreports, function(item, index) {
                myteamemployees.push(item);
              });

            }

            $scope.myteamemployees = myteamemployees;
          });
          console.log("My Team Employees", $scope.myteamemployees);
          //$scope.myteamemployees = response.data[0].Directreports;
          $ionicLoading.hide();
        }, function errorCallback(response) {
          alert("error");
          $ionicLoading.hide();
        });

      });

    }


    $scope.criteriaMatch = function(value, index, array) {
      if ($scope.searchval == null) {
        return true;
      } else {
        var searchInput = $scope.searchval.toLowerCase();
        if ((value.fullName.toLowerCase().indexOf($scope.searchInput) !== -1) || (value.id.indexOf($scope.searchInput) !== -1) || (value.email.toLowerCase().indexOf($scope.searchInput) !== -1)) {

          return true;
        } else {
          return false;
        }

        //return (value.fullName.search( $scope.searchval ) || value.id.search( $scope.searchval ) || value.email.search( $scope.searchval ));  
      }
    }


    $scope.search = function() {
      $ionicLoading.show({
        content: 'Loading Search Results',
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
      });
      console.log("Searched Input:", this.searchval);
      $scope.searchval = this.searchval;
      var pattern = /^(0|[1-9][0-9]{0,2}(?:(,[0-9]{3})*|[0-9]*))(\.[0-9]+){0,1}$/;
      if(pattern.test($scope.searchval)) {
          var searchUrl = final_baseUrl + "Employees/SearchEmployees?employeeid=" + this.searchval;
      }
      else{
           var searchUrl = final_baseUrl + "Employees/SearchEmployees?name=" + this.searchval;
      }
    
      AuthenticationService.getTokenFromStorage(token_key).then(function(access_token) {
        $http.defaults.headers.common['Authorization'] = 'Bearer ' + access_token;
        $http({
          method: 'GET',
          url: searchUrl
        }).then(function successCallback(response) {
          console.log("Search Results From API", response);
          $scope.globalemployees = response.data;

          $ionicLoading.hide();
          if ($scope.globalemployees.length == 0) {
            $ionicPopup.alert({
              title: 'No Results Found'
            })
          }
        }, function errorCallback(response) {
          alert("Error while calling the Employee Lookup API");
          $ionicLoading.hide();
        });
      });

    }




    $scope.viewMyTimeOff = function() {
      $state.go('app.section_one_a1.details', {
        id: $rootScope.employeeid,
        name: $scope.myfirstName + " " + $scope.mylastName
      });
    }


    $scope.showEmpDetail = function(employee) {
      $state.go('app.empsearch.empdetail', {
        selected_employee: employee
      });
    }

  })



  //Employee Details Controller

  .controller('EmployeeDetailCtrl', function($scope,$http, $stateParams, $state, $rootScope, AuthenticationService,$ionicLoading) {
    $scope.employee = $stateParams.selected_employee;
    $scope.employee.Manager = $scope.employee.managerInfo;
    console.log("Selected Employee", $scope.employee);

    $scope.showFab = false;
    $scope.showTimeOff = false;
    $scope.showdirectReports = false;

    if ($scope.employee.DirectReportsCount > 0) {

      $scope.showdirectReports = true;
      $scope.showFab = true;

    }

    if ($scope.employee.Manager != null) {
      if ($scope.employee.Manager.managerId == $rootScope.employeeid) {
        $scope.showTimeOff = true;
      }
    }


    /*********************Check isManagerHierarchy to display PTO******************/
     if(!$rootScope.demo_mode){

     var token_key = "access_token";
     var employeeid = $rootScope.employeeid;
    
     var isManagerUrl = final_baseUrl + "Employees/isManagerInHierarchy?underling_empno=" + $scope.employee.id + "&mgr_empno=" + employeeid;
      AuthenticationService.getTokenFromStorage(token_key).then(function(access_token) {
        $http.defaults.headers.common['Authorization'] = 'Bearer ' + access_token;
        $http({
          method: 'GET',
          url: isManagerUrl
        }).then(function successCallback(response) {
          console.log("Search Results From isManager API", response.data);
          if(response.data){
            $scope.showTimeOff = true;

          }
          else{
            $scope.showTimeOff = false;
          }

           if ($scope.showdirectReports == true || $scope.showTimeOff == true) {
               $scope.showFab = true;
          }

          $ionicLoading.hide();
         
          
        }, function errorCallback(response) {
          alert("Error while calling the Employee Lookup API");
           $ionicLoading.hide();
        });
      });
       }
    /******************************************************************************/

    if ($scope.employee.id == $rootScope.employeeid) {
      $scope.showTimeOff = true;
      $scope.showFab = true;
    }

   


    console.log($scope.showFab);
    $scope.groups = [];
    for (var i = 0; i < 1; i++) {
      $scope.groups[i] = {
        name: $scope.employee.Manager,
        items: [],
        show: false
      };
    }

    $scope.toggleFab = function() {
      if (!$scope.fab) {
        $scope.fab = true;
      } else {
        $scope.fab = false;
      }

    }

    $scope.toggleGroup = function(group) {
      group.show = !group.show;
    };
    $scope.isGroupShown = function(group) {
      return group.show;
    };


    $scope.viewTimeOff = function() {
      $state.go('app.section_one_a1.details', {
        selected_employee: $stateParams.selected_employee
      });
    }

    $scope.viewDirectReports = function() {
      $state.go('app.section_one_a2.details', {
        id: $scope.employee.id,
        name: $scope.employee.firstName
      });
    }
  })


  .controller('SectionOne_A1', function($scope, $stateParams) {


  })


  .controller('SectionOne_A2', function($scope, $stateParams) {


  })


  .controller('DirectReportsCtrl', function($scope, $stateParams, $http, $state, $ionicLoading) {
    $scope.title = $stateParams.name + "'s" + " " + "Direct Reports";
    $ionicLoading.show({
      content: 'Loading Search Results',
      animation: 'fade-in',
      showBackdrop: true,
      maxWidth: 200,
      showDelay: 0
    });
    var url = final_baseUrl + "Employees/getEmployees?searchstring=" + $stateParams.id + "&showdirectReports=X";
    $http.get(url).then(function(results) {
      console.log("DirectReportsData", results);
      $scope.mydirectemployees = results.data[0].Directreports;
      $ionicLoading.hide();
    });

    $scope.showEmpDetail = function(employee) {
      $state.go('app.empsearch.empdetail', {
        selected_employee: employee
      });
    }

  })

  .controller('TimeOffCtrl', function($scope, $stateParams, $http, $ionicHistory, $ionicLoading, $state, $rootScope ) {
    window.ga.trackView('Viewed Time Off');
    $ionicLoading.show({
      content: 'Loading Search Results',
      animation: 'fade-in',
      showBackdrop: true,
      maxWidth: 200,
      showDelay: 0
    });
    console.log("StateParams", $stateParams);
    window.ga.trackEvent('TimeOff', 'View', 'EmpID', $stateParams.id);

    if ($stateParams.id != null) {
      $scope.employeeName = $stateParams.name;
      $scope.employeeid = $stateParams.id;
      $scope.pto_header = 'My PTO';

    }

    if ($stateParams.selected_employee != null) {
      $scope.employeeName = $stateParams.selected_employee.firstName;
      $scope.employeeid = $stateParams.selected_employee.id;
      $scope.pto_header = $scope.employeeName + "'s PTO";
    }



    /* Call Get PTO  */
    if($rootScope.demo_mode){
      var url = "feeds/timeoff.json";
      $scope.pto_header = "My PTO";
    }

    else{
       var url = final_baseUrl + "Employees/getPTO?emloyeeid=" + $scope.employeeid;
    }

   
    $http.get(url).then(function(result) {
      console.log("Get PTO API DATA", result.data);
      $scope.pto_details = result.data;
      $ionicLoading.hide();
    });




    $scope.filterFn = function(pto_detail) {
      // Do some tests

      if (pto_detail.entitled > 0) {
        return true; // this will be listed in the results
      }

      return false; // otherwise it won't be within the results
    };

    $scope.goBack = function() {
      if ($stateParams.id != null || $rootScope.demo_mode) {
        $ionicHistory.goBack();
      } else {
        $state.go('app.empsearch.empdetail', {
          selected_employee: $stateParams.selected_employee
        });
      }

    }

  })

  /////Nature Clock Controller
  .controller('NatureClockCtrl', function($scope, $http, $ionicModal, $ionicLoading, $localStorage,
    $state, FavoritesService, NCAPIService, $rootScope, $ionicPopup, AuthenticationService) {
    window.ga.trackView('Nature Clock');
    console.log("Inside Nature Clock");
    var natureclockURL = "https://natureclock.daveyinstitute.com";
    var session_cookie;


    if ($localStorage.currentLocation && $localStorage.currentLocationCode) {
      console.log("Saved Location", $localStorage.currentLocation);
      $scope.isCurrentLocation = true;
      $scope.currentLocationCode = $localStorage.currentLocationCode;
      $scope.currentLocation = $localStorage.currentLocation;
    }
    $scope.states = [];
    $ionicModal.fromTemplateUrl('templates/search_locations.html', {
      scope: $scope,
      animation: 'slide-in-up',
    }).then(function(modal) {
      $scope.modal = modal;
    });

    $scope.openLocationsModal = function() {

      if ($scope.states.length == 0) {
        $ionicLoading.show({
          template: '<ion-spinner icon="bubbles"></ion-spinner><p>Fetching Locations...</p>'
        });
      
      
        /*   var url_list = NCAPIService.getURL($rootScope.demo_mode, null); */
        var locationurl = final_baseUrl + 'NatureClock/GetListOfLocations';

        NCAPIService.getAllLocationsFromAPI(locationurl).then(function(statesdata) {
          console.log("Locations From NC New API", statesdata);
          $scope.states = statesdata;
          $ionicLoading.hide();
        });
      }
      $scope.modal.show().then(function() {

        $scope.fadeBuildForm = true;
      })
    };



    $scope.setLocation = function(location) {
      console.log("Location Set To", location);
      $scope.currentLocation = location.cityName;
      $scope.currentLocationCode = location.locationCode;
      $scope.isCurrentLocation = true;
      $localStorage.currentLocation = $scope.currentLocation;
      $localStorage.currentLocationCode = $scope.currentLocationCode;
      $scope.modal.hide();
    }

    $scope.currentfilter = "Insects";

    $scope.newValue = function(currentfilter) {
      $scope.currentfilter = currentfilter;
      console.log("Current Filter", $scope.currentfilter);
    }

    $scope.displayResults = function() {
      
     
      $ionicLoading.show({
        template: '<ion-spinner icon="bubbles"></ion-spinner><p>Fetching Results...</p>'
      });
      //var url_list = NCAPIService.getURL($rootScope.demo_mode, $localStorage.currentLocationCode);
      var url = final_baseUrl + 'NatureClock/getNatureClockOutput?locationcode=' + $localStorage.currentLocationCode;
      NCAPIService.getAllDataFromAPI(url, $localStorage.currentLocationCode).then(function(data) {
        $ionicLoading.hide();
        console.log("Get all Data From API", data);
        var naturedata = {
          insects: data.insects,
          plants: data.plants
        };
        $rootScope.NC_currentYear = data.currentYear;


        if ($scope.currentfilter == "Insects") {
          $state.go('app.insects', {
            "naturedata": naturedata
          });
        } else {
          $state.go('app.plants', {
            "naturedata": naturedata
          });
        }
      });

    }

    $scope.openFavorites = function() {
      $scope.fab = false;
      $state.go('app.naturefavorites');
    }

    $scope.openCalendar = function() {
      $scope.fab = false;
      $ionicPopup.alert({
        title: 'Calendar View',
        template: 'The Functionality is not yet Available!'
      });
    }

    $scope.fab = false;

    $scope.toggleFab = function(fab) {
      $scope.fab = !$scope.fab;
    }



  })
  ////

  .controller('NatureInsectCtrl', function($scope, $http, $ionicModal, $ionicLoading, $localStorage,
    $state, $rootScope, FavoritesService, NCAPIService, $stateParams) {


    $scope.insects = $stateParams.naturedata.insects.Category;
    $scope.plants = $stateParams.naturedata.plants.Plant;

    $scope.setInsect = function(insect, insectcategory) {
      console.log("insect category", insectcategory);
      console.log("insect", insect);
      $state.go('app.natureinsectdetail', {
        "year": $rootScope.NC_currentYear,
        "category": insectcategory.Name,
        "insect": JSON.stringify(insect)
      });
    }

    $scope.setPlant = function(plant) {
      console.log("SelectedPlant", plant);
      $state.go('app.natureplantdetail', {
        "plant": JSON.stringify(plant)
      });
    }
  })

  .controller('NatureInsectDetailsCtrl', function($scope, $stateParams, $localStorage, $rootScope, FavoritesService, $ionicModal, $ionicSlideBoxDelegate, $ionicScrollDelegate) {

    $scope.selectedInsect = JSON.parse($stateParams.insect);
    console.log("selectedInsect", $scope.selectedInsect);
    console.log("StateParams", $stateParams);
    $scope.selectedInsectCat = $stateParams.category;
    $scope.currentYear = $stateParams.year;
    $scope.affectedPlants = $scope.selectedInsect.AffectedPlants;
    if ($scope.selectedInsect.Pictures == "") {
      $scope.pictures = [];
    } else {
      var str_pictures = JSON.stringify($scope.selectedInsect.Pictures);
      var replaced = str_pictures.replace(/http/g, 'https');
      var newPictures = JSON.parse(replaced);
      $scope.pictures = newPictures.Picture;
    }
    console.log("Pictures", $scope.pictures);
    console.log("AffectedPlants", $scope.affectedPlants);
    $scope.sectionMonitoring = false;
    $scope.sectionProactive = false;
    $scope.sectionExtended = false;
    $scope.sectionTreatment = false;
    $scope.sectionPeriods = false;
    $scope.sectionAffected = false;
    $scope.sectionPictures = false;
    $scope.sectionRecommended = false;

    $scope.heartFilled = FavoritesService.insects.findFavItemByNameAndLocation($scope.selectedInsect.Name, $localStorage.currentLocationCode);
    console.log("HeartFilled?", $scope.heartFilled);

    $scope.toggleLike = function() { //this gets called when user clicks on the heart button
      if ($scope.heartFilled) {
        $scope.heartFilled = false;
        var removed = FavoritesService.insects.removeFavoriteItems($scope.selectedInsect.Name, $localStorage.currentLocationCode);
        if (removed) {
          window.plugins.toast.showWithOptions({
            message: "Removed From Favorites",
            duration: "short", // which is 2000 ms. "long" is 4000. Or specify the nr of ms yourself.
            position: "center",
            addPixelsY: -40 // added a negative value to move it up a bit (default 0)
          });
        }

      } else {
        $scope.heartFilled = true;
        var added = FavoritesService.insects.addFavoriteItems($scope.selectedInsect.Name, $localStorage.currentLocationCode)
        if (added) {
          window.plugins.toast.showWithOptions({
            message: "Added To Favorites",
            duration: "short", // which is 2000 ms. "long" is 4000. Or specify the nr of ms yourself.
            position: "center",
            addPixelsY: -40 // added a negative value to move it up a bit (default 0)
          });
        }
      }
    } // End of Toggle Like Button  

    $scope.zoomMin = 1;
    $scope.showImages = function(index) {
      $scope.activeSlide = index;
      $scope.showModal('templates/gallery_view.html');
    };

    $scope.showModal = function(templateUrl) {
      $ionicModal.fromTemplateUrl(templateUrl, {
        scope: $scope
      }).then(function(modal) {
        $scope.modal = modal;
        $scope.modal.show();
      });
    }

    $scope.closeModal = function() {
      $scope.modal.hide();
      $scope.modal.remove()
    };

    $scope.updateSlideStatus = function(slide) {
      var zoomFactor = $ionicScrollDelegate.$getByHandle('scrollHandle' + slide).getScrollPosition().zoom;
      if (zoomFactor == $scope.zoomMin) {
        $ionicSlideBoxDelegate.enableSlide(true);
      } else {
        $ionicSlideBoxDelegate.enableSlide(false);
      }
    };
    $scope.zoomPicture = function(photo) {
      console.log("FULL View");
      $scope.selectedInsectPhoto = $scope.pictures[photo];
      console.log("Selected Photo", $scope.selectedInsectPhoto);
      $scope.viewingFull = !$scope.viewingFull;
    }



  })

  .controller('NaturePlantDetailsCtrl', function($scope, $stateParams, $localStorage, $rootScope, FavoritesService) {

    $scope.selectedPlant = JSON.parse($stateParams.plant);
    console.log("selectedPlant", $scope.selectedPlant);
    $scope.currentYear = $stateParams.year;
    $scope.heartFilled = FavoritesService.plants.findFavItemByNameAndLocation($scope.selectedPlant.Name, $localStorage.currentLocationCode);


    $scope.toggleLike = function() { //this gets called when user clicks on the heart button
      if ($scope.heartFilled) {
        $scope.heartFilled = false;
        var removed = FavoritesService.plants.removeFavoriteItems($scope.selectedPlant.Name, $localStorage.currentLocationCode);
        if (removed) {
          window.plugins.toast.showWithOptions({
            message: "Removed From Favorites",
            duration: "short", // which is 2000 ms. "long" is 4000. Or specify the nr of ms yourself.
            position: "center",
            addPixelsY: -40 // added a negative value to move it up a bit (default 0)
          });
        }

      } else {
        $scope.heartFilled = true;
        var added = FavoritesService.plants.addFavoriteItems($scope.selectedPlant.Name, $localStorage.currentLocationCode)
        if (added) {
          window.plugins.toast.showWithOptions({
            message: "Added To Favorites",
            duration: "short", // which is 2000 ms. "long" is 4000. Or specify the nr of ms yourself.
            position: "center",
            addPixelsY: -40 // added a negative value to move it up a bit (default 0)
          });
        }
      }
    } // End of Toggle Like Button  

  })

  .controller('NatureFavoritesCtrl', function($scope, $localStorage, $rootScope, FavoritesService, NCAPIService, $state, $ionicPopup, $ionicLoading) {

   
  

    $scope.nature_insectfavorites = FavoritesService.insects.getAllFavoriteItems();
    $scope.nature_plantfavorites = FavoritesService.plants.getAllFavoriteItems();
    console.log("insect Favorites", $scope.nature_insectfavorites);
    console.log("plant Favorites", $scope.nature_plantfavorites);

    $scope.insectLocations = _.keys(_.countBy($scope.nature_insectfavorites, function(data) { return data.location; }));
    console.log("Unique Insect Locations", $scope.insectLocations);

    $scope.plantLocations = _.keys(_.countBy($scope.nature_plantfavorites, function(data) { return data.location; }));
    console.log("Unique Plant Locations", $scope.plantLocations);

    $scope.setFavInsect = function(insect) {
      $ionicLoading.show({
        template: '<ion-spinner icon="bubbles"></ion-spinner><p>Fetching Insect Details...</p>'
      });
      // var url_list = NCAPIService.getURL($rootScope.demo_mode, insect.location);
      //  var url = url_list.naturedata_url;
      var url = final_baseUrl + 'NatureClock/getNatureClockOutput?locationcode=' + insect.location;
      NCAPIService.getInsectFromNameandLocation(insect.name, url, insect.location).then(function(insectFound) {
        console.log("insectFound From API", insectFound);
        $ionicLoading.hide();
        $state.go('app.natureinsectdetail', {
          "year": insectFound.currentYear,
          "category": insectFound.category,
          "insect": JSON.stringify(insectFound.insect)
        });


      });


    }

    $scope.setFavPlant = function(plant) {
      $ionicLoading.show({
        template: '<ion-spinner icon="bubbles"></ion-spinner><p>Fetching Plant Details...</p>'
      });
      //var url_list = NCAPIService.getURL($rootScope.demo_mode, plant.location);
      // var url = url_list.naturedata_url;
      
   
      var url = final_baseUrl + 'NatureClock/getNatureClockOutput?locationcode=' + plant.location;
      NCAPIService.getPlantFromNameandLocation(plant.name, url, plant.location).then(function(plantFound) {
        console.log("plantFound From API", plantFound);
        $ionicLoading.hide();
        $state.go('app.natureplantdetail', {
          "year": plantFound.currentYear,
          "plant": JSON.stringify(plantFound.plant)
        });


      });


    }

    $scope.deleteAllFavorites = function() {
      var confirmPopup = $ionicPopup.confirm({
        title: 'Do You want to Delete All the Favorites',
        cancelText: 'Cancel',
        okText: 'Yes'
      }).then(function(res) {
        if (res) {
          $localStorage.nc_favorites = [];
          $state.reload();
        }
      });

    }

  })

  .controller('ClientLookupCtrl', function($scope, $state, $http, $rootScope, $ionicLoading, clientResultService, $timeout, AuthenticationService, empSalesOfficeService, $ionicHistory, $ionicModal) {
    var employeeid = $rootScope.employeeid;
    var costcenter = $rootScope.costcenter;
    var salesperson = $rootScope.employeeid;
     
    if (employeeid == '20055710' || employeeid == '20044002') {
       costcenter = '133131';
       employeeid = '898775';
        salesperson = '650929';
    }


     /* $ionicLoading.show({
      template: '<ion-spinner name="bubbles"></ion-spinner> '
    });

   var token_key = 'access_token';
    AuthenticationService.getTokenFromStorage(token_key).then(function(access_token) {
      $http.defaults.headers.common['Authorization'] = 'Bearer ' + access_token;

     
      var url_getSalesOffice = final_baseUrl + 'Employees/getAllSalesOfficesForEmployee?employeeid=' + employeeid;

      $http({
        method: 'GET',
        url: url_getSalesOffice
      }).then(function successCallback(response) {
        console.log("Get Sales Office Response", response.data);
        // $rootScope.officeNo = response.data.officeno;
        // $rootScope.officeDesc = response.data.officeName;
        empSalesOfficeService.empSalesOfficeList = response.data;
        if (empSalesOfficeService.empSalesOfficeList.length == 0) {
          alert("Error fetching Sales Office For the Logged In User. Kindly, check with Davey Mobile App Team whether you are allowed to use this app");
          $ionicLoading.hide();
          $ionicHistory.nextViewOptions({
            historyRoot: true
          });
          $state.go('app.dashboard');
        } else {
          $ionicLoading.hide();
          var selectedSalesOffice = empSalesOfficeService.empSalesOfficeList[0];
          $scope.officeNo = $rootScope.officeNo = selectedSalesOffice.officeno;
          $scope.officeDesc = $rootScope.officeDesc = selectedSalesOffice.officeName;
        }



      }, function errorCallback(response) {
        alert("Error fetching Sales Office For the Logged In User. Kindly, check with Davey Mobile App Team whether you are allowed to use this app");
        $ionicLoading.hide();
        $ionicHistory.nextViewOptions({
          historyRoot: true
        });
        $state.go('app.dashboard');
      });

    }); */

    $scope.officeNo =  $rootScope.officeNo;
     $scope.officeDesc = $rootScope.officeDesc;



    $scope.goTo = function(mystate) {
      console.log("state", mystate);
      $state.go(mystate);
    }






    $scope.goToMyClients = function() {
      $ionicLoading.show();
      var testurl = "feeds/clients_list.json";
      var token_key = 'access_token';
      AuthenticationService.getTokenFromStorage(token_key).then(function(access_token) {
        $http.defaults.headers.common['Authorization'] = 'Bearer ' + access_token;

        function alertDismissed() {
          console.log("No Results for My Clients");
        }

        var url_getMyCustomers = final_baseUrl + 'Customers/getMyCustomers?salesperson_id=' + salesperson;


        $http({
          method: 'GET',
          url: url_getMyCustomers
        }).then(function successCallback(response) {
          console.log("Get Search Customers Response", response.data);
          $scope.myClients = response.data;
          $ionicLoading.hide();
          if ($scope.myClients.length == 0) {
            navigator.notification.alert(
              'There are No Customers who you have worked with. Kindly, use the Search Functionality To Find Customers In Your Sales Office', // message
              alertDismissed, // callback
              'No Records Found', // title
              'OK' // buttonName
            );
          } else {
            clientResultService.clientList = response.data;
            $state.go('app.client_search_result');
          }


        }, function errorCallback(response) {
           navigator.notification.alert("This App is Only Accessible by users having a Sales Office ");
          $ionicLoading.hide();
        });

      });
    }
  })

  .controller('ClientSearchCtrl', function($scope, $state, $ionicScrollDelegate, $location, $ionicHistory, $ionicModal, $http, $ionicLoading, $rootScope, AuthenticationService,
    clientResultService, yearRangeService,empSalesOfficeService, $ionicHistory) {

   
    if ($rootScope.employeeid == '20055710' || $rootScope.employeeid == '20044002') {
      
  //    $rootScope.costcenter = '133131';
    }

    $ionicModal.fromTemplateUrl("templates/Client_Lookup/sales_office_modal.html", {
      scope: $scope
    }).then(function(modal) {
      $scope.salesOfficeModal = modal;

    });

    $scope.salesOfficeList = empSalesOfficeService.empSalesOfficeList;


    $scope.openSalesOfficeModal = function() {

      $scope.salesOfficeModal.show().then(function() {
        $scope.fadeBuildForm = true;
      })
    }

    $scope.setSalesOffice = function(soffice) {
      $scope.officeNo = $rootScope.officeNo = soffice.officeno;
      $scope.officeDesc = $rootScope.officeDesc = soffice.officeName;
      $scope.salesOfficeModal.hide();
    }




    $scope.officeno = $rootScope.officeNo;
    $scope.officeDesc = $rootScope.officeDesc;

    var d = new Date();
    var currentYear = d.getFullYear();
    var fromYear = d.getFullYear() - 3;
    $scope.searchForm = { fromYear: fromYear, toYear: currentYear };



    $scope.scrollToElement = function(e) {

      $ionicScrollDelegate.$getByHandle('mainScroll').anchorScroll("#" + $location.hash(e.target.id));
    }

    $scope.goBack = function() {
      $state.go('app.client_lookup');
    }

    $scope.resetForm = function() {
      $scope.blankForm = {};
      $scope.searchForm = angular.copy($scope.blankForm);

      $scope.searchForm.fromYear = (new Date()).getFullYear() - 3;
      $scope.searchForm.toYear = (new Date()).getFullYear();
    }




    $ionicModal.fromTemplateUrl('templates/Client_Lookup/search_arborists.html', {
      scope: $scope,
      animation: 'slide-in-up',
    }).then(function(modal) {
      $scope.ArboristModal = modal;
    });

    $scope.openArboristsModal = function() {
      $ionicLoading.show();
      var token_key = 'access_token';
      var url_getArborists = final_baseUrl + 'Customers/getArboristsFromSalesOffice?salesofficeno=' + $rootScope.officeNo;
      AuthenticationService.getTokenFromStorage(token_key).then(function(access_token) {
        $http.defaults.headers.common['Authorization'] = 'Bearer ' + access_token;

        $http({
          method: 'GET',
          url: url_getArborists
        }).then(function successCallback(response) {
          $ionicLoading.hide();
          $scope.arborists = response.data;
          $scope.totalArboristCount = $scope.arborists.length;
        }, function errorCallback(response) {
          alert("Unable to Load Arborists");

        });
        $scope.ArboristModal.show().then(function() {
          $scope.fadeBuildForm = true;
        })

      });

    }

    $scope.setarborist = function(arborist) {
      console.log("Selected arborist", arborist);

      $scope.selectedarborist = arborist.name;
      $scope.searchForm.selectedarboristname = $scope.selectedarborist;

      $scope.selectedarboristId = arborist.arborist_empno;
      $scope.searchForm.selectedarboristid = $scope.selectedarboristId;
      console.log("Search Form Data", $scope.searchForm);
      $scope.ArboristModal.hide();
    }

    $ionicModal.fromTemplateUrl('templates/Client_Lookup/search_states.html', {
      scope: $scope,
      animation: 'slide-in-up',
    }).then(function(modal) {
      $scope.StatesModal = modal;
    });

    $scope.openStatesModal = function() {
      $http({
        method: 'GET',
        url: 'feeds/states_list.json'
      }).then(function successCallback(response) {
        $scope.states = response.data;

      }, function errorCallback(response) {
        alert("Unable to Load States");

      });
      $scope.StatesModal.show().then(function() {
        $scope.fadeBuildForm = true;
      })
    }

    $scope.setState = function(state) {
      console.log("Selected state", state);
         $scope.searchForm.selectedState = state.stateCode;
      $scope.selectedState = state.stateCode;
      $scope.StatesModal.hide();
    }



    $scope.setFromYear = function(year) {
      console.log("Selected From Year", year);
      $scope.fromYear = year;
      $scope.YearsModal.hide();
    }

    $scope.setToYear = function(year) {
      console.log("Selected To Year", year);
      $scope.toYear = year;
      $scope.YearsModal.hide();
    }


    $scope.enableCustomers = { checked: true };
    $scope.enableProspects = { checked: true };



    $scope.goToSearchResults = function(searchFormData) {
      console.log("Form Data To be Sent", searchFormData);
      var validated = true;
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner> <br/> Fetching Customers Data....'
      });

      var token_key = 'access_token';
     
      var url_searchCustomers = final_baseUrl + 'Customers/SearchCustomers?officeNo=' +  $scope.officeNo;
      if (searchFormData.customerno) {
        url_searchCustomers = url_searchCustomers + '&custNo=' + searchFormData.customerno;
      }
      if (searchFormData.fullname) {
        url_searchCustomers = url_searchCustomers + '&name1=' + searchFormData.fullname;
      }
      if (searchFormData.houseno) {
        url_searchCustomers = url_searchCustomers + '&houseNo=' + searchFormData.houseno;
      }
      if (searchFormData.street) {
        url_searchCustomers = url_searchCustomers + '&street=' + searchFormData.street;
      }
      if(searchFormData.city)
      {
         url_searchCustomers = url_searchCustomers + '&city=' + searchFormData.city;
      }
      if (searchFormData.zip) {
        url_searchCustomers = url_searchCustomers + '&zip=' + searchFormData.zip;
      }
      if (searchFormData.selectedState) {
        url_searchCustomers = url_searchCustomers + '&state=' + searchFormData.selectedState;
      }
      if (searchFormData.searchterm) {
        url_searchCustomers = url_searchCustomers + '&searchTerm=' + searchFormData.searchterm;
      }
      if (searchFormData.phone) {
        url_searchCustomers = url_searchCustomers + '&phone=' + searchFormData.phone;
      }
      if (searchFormData.email) {
        url_searchCustomers = url_searchCustomers + '&emailAddress=' + searchFormData.email;
      }
      if (searchFormData.selectedarboristid) {
        url_searchCustomers = url_searchCustomers + '&salesperson=' + searchFormData.selectedarboristid;
      }

      if ($scope.enableProspects.checked && $scope.enableCustomers.checked) {
        url_searchCustomers = url_searchCustomers + '&custType=' + 'B';
      }

      if ($scope.enableProspects.checked && !$scope.enableCustomers.checked) {
        url_searchCustomers = url_searchCustomers + '&custType=' + 'P';
      }

      if ($scope.enableCustomers.checked && !$scope.enableProspects.checked) {
        url_searchCustomers = url_searchCustomers + '&custType=' + 'C';
      }


      if (searchFormData.toYear == "" || searchFormData.toYear == null) {
        searchFormData.toYear = currentYear;
      }

      if (searchFormData.fromYear == "" || searchFormData.fromYear == null) {
        searchFormData.fromYear = fromYear;
      }
      if (searchFormData.fromYear > searchFormData.toYear) {
        validated = false;
        var alertPopup = $ionicPopup.alert({
          title: 'Form Validation Error',
          template: 'From Year cannot be Bigger Than To Year'
        });

        alertPopup.then(function(res) {
          console.log('Year Range Error');
        });
      }


      function onConfirm(buttonIndex) {
        console.log("Button Index Selected", buttonIndex);
        if (buttonIndex == 2) {
          clientResultService.clientList = $scope.clientResults;
          $state.go('app.client_search_result');

        }
        $ionicLoading.hide();

      }

      if (validated) {
        AuthenticationService.getTokenFromStorage(token_key).then(function(access_token) {
          $http.defaults.headers.common['Authorization'] = 'Bearer ' + access_token;

          console.log("URL For Search Customers", url_searchCustomers);

          function alertDismissed() {
            console.log("Zero Results Fetched");
          }


          $http({
            method: 'GET',
            url: url_searchCustomers
          }).then(function successCallback(response) {
            console.log("Get Search Customers Response", response.data);
            $scope.myClients = response.data;
            if ($scope.myClients.length == 0) {

              navigator.notification.alert(
                'The Search Returned No Results. Kindly, refine the form parameters and search again.', // message
                alertDismissed, // callback
                'No Records Found', // title
                'Search Again' // buttonName

              );
              $ionicLoading.hide();
            }
            $scope.clientResults = response.data;
            if ($scope.myClients.length >= 500) {
              navigator.notification.confirm(
                'Search Criteria Too Broad. Results Restricted to 500 Records. Please refine your search criteria to view more accurate results ', // message
                onConfirm, // callback to invoke with index of button pressed
                'Warning', // title
                ['Refine Search', 'Continue'] // buttonLabels
              );
            }

            if ($scope.myClients.length > 0 && $scope.myClients.length < 500) {
              $scope.clientResults = response.data;
              clientResultService.clientList = $scope.clientResults;
              $state.go('app.client_search_result');
              $ionicLoading.hide();
            }


            yearRangeService.fromYear = searchFormData.fromYear;
            yearRangeService.toYear = searchFormData.toYear;




          }, function errorCallback(response) {
            alert("Error fetching Customers from Database. Please contact Davey Mobile App Team for further assistance");
            $ionicLoading.hide();

                $ionicHistory.nextViewOptions({
                  historyRoot: true
                });
                $state.go('app.dashboard');
          });

        });

      }

    }



    if (ionic.Platform.isAndroid()) {
      StatusBar.backgroundColorByHexString("#35a56b");
    }

    $scope.$on('$ionicView.beforeLeave', function(e) {
      if (ionic.Platform.isAndroid()) {
        StatusBar.backgroundColorByHexString("#00833F");
      }
    });

  })


  .controller('ClientSearchResultsCtrl', function($scope, $http, $rootScope, $state, $ionicLoading, $state, clientResultService, $ionicPopup, clientService, $ionicHistory) {


    if ($rootScope.employeeid == '20055710' || $rootScope.employeeid == '20044002') {
      $scope.employeeid = '650929';
    } else {
      $scope.employeeid = $rootScope.employeeid;
    }
    /* var url = "feeds/clients_list.json";
     $http.get(url).then(function(data) {
       var myclients = data.data;

       $scope.myClients = myclients;
       console.log("Client Results List", $scope.myClients);
     });*/



    $scope.myClients = clientResultService.clientList;
    console.log("Client Results From Service", $scope.myClients);
    var views = $ionicHistory.viewHistory().views;
    var Histories = $ionicHistory.viewHistory().histories;
    console.log("Views", views);
    console.log("Histories", Histories);





    $scope.goBack = function() {
       $ionicHistory.nextViewOptions({ disableBack: true, disableAnimate: true, historyRoot: false });
         $state.go('app.client_search');
      
    }

    var latitude, longitude;
    $scope.showClientDetail = function(client) {
      var fullAddress = client.houseno + ' ' + client.street + ' ' + client.city +
        ' ' + client.state + ' ' + client.zip;
      console.log("Full Address to be Geocoded", fullAddress);
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner> <br/> Fetching Customer Details'
      });
      nativegeocoder.forwardGeocode(success, failure, fullAddress, { useLocale: true, maxResults: 1 });

      function success(coordinates) {
        //alert("The coordinates are latitude = " + coordinates[0].latitude + " and longitude = " + coordinates[0].longitude);
        latitude = coordinates[0].latitude;
        longitude = coordinates[0].longitude;
        client.latitude = latitude;
        client.longitude = longitude;
        console.log("Client to be sent", client);
        $ionicLoading.hide();
        clientService.client = client;
        $state.go('tab.basicinfo', {
          "client": JSON.stringify(client)
        });
      }

      function failure(err) {
        alert("The Address Could Not Be Verified. The Maps Functionality Won't Work");
        $ionicLoading.hide();
        $state.go('tab.basicinfo', {
          "client": JSON.stringify(client)
        });
      }
    }
  })


  .controller('Client_MapsInfoCtrl', function($scope, clientService, $window, $timeout) {
    $scope.test = function() {

      console.log('template compiled', clientService.client);
    }


    $scope.selectedClient = clientService.client;
    var latitude = parseFloat($scope.selectedClient.latitude);
    var longitude = parseFloat($scope.selectedClient.longitude);
    var address = $scope.selectedClient.houseno + ' ' + $scope.selectedClient.street + ',' + $scope.selectedClient.city + ',' + $scope.selectedClient.state + ' ' + $scope.selectedClient.zip;
    console.log("Inside Maps Control", clientService.client);


    angular.extend($scope, {
      center: {
        lat: latitude,
        lng: longitude,
        zoom: 1
      },
      markers: {
        m1: {
          lat: latitude,
          lng: longitude,
          message: address,
          focus: true,
          icon: {},
        },
      },
      defaultIcon: {},
      leafIcon: {
        iconUrl: 'img/marker/' + $scope.selectedClient.custType + '.png',
        shadowUrl: 'img/marker/marker-shadow.png',
        iconSize: [25, 39], // size of the icon
        shadowSize: [50, 64], // size of the shadow
        iconAnchor: [12, 38], // point of the icon which will correspond to marker's location
        shadowAnchor: [4, 62], // the same for the shadow
        popupAnchor: [-3, -46] // point from which the popup should open relative to the iconAnchor
      },
      defaults: {
        maxZoom: 22,
        minZoom: 14,
        tileLayer: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
        tileLayerOptions: {
          opacity: 0.9,
          detectRetina: false,
          reuseTiles: true,
        },
        scrollWheelZoom: false
      }
    });

    $scope.markers.m1.icon = $scope.leafIcon;



    $scope.openNavigator = function() {
      var geoString = '';

      if (ionic.Platform.isIOS()) {
        geoString = 'maps://?q=' + $scope.selectedClient.latitude + ',' + $scope.selectedClient.longitude + '';
      } else if (ionic.Platform.isAndroid()) {
        geoString = 'geo://?q=' + $scope.selectedClient.latitude + ',' + $scope.selectedClient.longitude + '';
      }
      window.open(geoString, '_system');
    }

  })

  .controller('ClientHistoryCtrl', function($scope, clientService, $state, $ionicModal, AuthenticationService, $ionicLoading, yearRangeService, $http, $rootScope, $ionicScrollDelegate, $ionicPlatform, $ionicHistory) {
    
     
   
    //Override the Hardware Back Button
    var doCustomBack = function() {
      $state.go('app.client_search_result');
    };

    // registerBackButtonAction() returns a function which can be used to deregister it
    var deregisterHardBack = $ionicPlatform.registerBackButtonAction(
      doCustomBack, 101
    );

    $scope.$on('$destroy', function() {
      deregisterHardBack();
    });

    $scope.active_segment = 1;
    $scope.activate_segment = function(index) {
      $scope.active_segment = index;
      $ionicScrollDelegate.resize();
    };

    var client = clientService.client;

   


    $scope.$on('$ionicView.afterEnter', function() {
      console.log("Inside Client History Control", clientService.client);

      if (!yearRangeService.fromYear && !yearRangeService.toYear) {
        yearRangeService.fromYear = (new Date()).getFullYear() - 3;
        yearRangeService.toYear = (new Date()).getFullYear();
      }
      console.log("Year Range Service", yearRangeService);
      $ionicLoading.show();
      var token_key = 'access_token';
      AuthenticationService.getTokenFromStorage(token_key).then(function(access_token) {
        var url_getContracts = final_baseUrl + 'Customers/getContractsHistory?custNo=' + client.custno + '&officeNo=' + client.officeno + '&startYr=' +
          yearRangeService.fromYear + '&endYr=' + yearRangeService.toYear;
        console.log("Get Contracts URL", url_getContracts);

        $http.defaults.headers.common['Authorization'] = 'Bearer ' + access_token;
        $http({
          method: 'GET',
          url: url_getContracts
        }).then(function successCallback(response) {
          console.log("Get Contracts History Response", response.data);
          $scope.contracts_history = response.data;
          $ionicLoading.hide();
        }, function errorCallback(response) {
          alert("Error fetching Customers from Database. Please Close the App and ReLogin");
          $ionicLoading.hide();
              $ionicHistory.nextViewOptions({
                historyRoot: true
              });
        $state.go('app.dashboard');
          
        });

      });
    });


    $scope.viewTexts = function(contract) {

      $ionicLoading.show();
      var token_key = 'access_token';
      AuthenticationService.getTokenFromStorage(token_key).then(function(access_token) {
        var url_getContract_Texts = final_baseUrl + 'Customers/getContractItemTexts?salesofficeno=' + contract.officeno + '&contractno=' + contract.docno + '&itemno=' + contract.itemNo;
        console.log("Get Contracts Texts URL", url_getContract_Texts);
        $http.defaults.headers.common['Authorization'] = 'Bearer ' + access_token;
        $http({
          method: 'GET',
          url: url_getContract_Texts
        }).then(function successCallback(response) {
          console.log("Get Contracts History Response", response.data);
          $scope.contracts_texts = response.data;
          $ionicLoading.hide();
          openTextsModal();
        }, function errorCallback(response) {
          alert("Error fetching Customers from Database. Please Close the App and ReLogin");
          $ionicLoading.hide();
          $state.go('app.dashboard');
        });

      });


    }

    $ionicModal.fromTemplateUrl('templates/Client_Lookup/Tabs/view_contract_texts_modal.html', {
      scope: $scope,
      animation: 'slide-in-up',
    }).then(function(modal) {
      $scope.modal = modal;
    });


    function openTextsModal() {
      $scope.modal.show().then(function(data) {
        console.log("Modal Opened");
      })
    }
    $scope.goBack = function() {
      $state.go('app.client_search_result');
    }

  })
  .controller('ClientDetailsCtrl', function($scope, $stateParams, $rootScope, clientService, $state) {
    $scope.minzoom = 12;
    $scope.maxzoom = 17;
    $scope.startzoom = 12;
    if ($stateParams.client) {
      $scope.selectedClient = JSON.parse($stateParams.client);
      $scope.employeeid = $rootScope.employeeid;
      console.log("Selected Client", $scope.selectedClient);
      clientService.client = $scope.selectedClient;
    } else {
      $scope.selectedClient = clientService.client;
    }


    if ($scope.selectedClient.comments != "") {
      $scope.customerComments = true;
    }


    $scope.contact_groups = [];
    $scope.comments_groups = [];
    $scope.general_groups = [];
    for (var i = 0; i < 1; i++) {
      $scope.general_groups[i] = {
        name: $scope.selectedClient.name1,
        items: [],
        show: true
      };
      $scope.contact_groups[i] = {
        name: $scope.selectedClient.name1,
        items: [],
        show: false
      };

      $scope.comments_groups[i] = {
        name: $scope.selectedClient.name1,
        items: [],
        show: false
      };


    }

    $scope.toggleGroup = function(group) {
      group.show = !group.show;
    };
    $scope.isGroupShown = function(group) {
      return group.show;
    };

    $scope.toggleLike = function() { //this gets called when user clicks on the heart button
      if ($scope.heartFilled) {
        $scope.heartFilled = false;
        if ($scope.selectedClient.custType == 'C') {
          var removed = FavoritesService.clients.removeFavoriteItems($scope.selectedClient.name1);
        } else {
          var removed = FavoritesService.prospects.removeFavoriteItems($scope.selectedClient.name1);
        }

        if (removed) {
          window.plugins.toast.showWithOptions({
            message: "Removed From Favorites",
            duration: "short", // which is 2000 ms. "long" is 4000. Or specify the nr of ms yourself.
            position: "center",
            addPixelsY: -40 // added a negative value to move it up a bit (default 0)
          });
        }

      } else {
        $scope.heartFilled = true;
        if ($scope.selectedClient.CustType == 'C') {
          var added = FavoritesService.clients.addFavoriteItems($scope.selectedClient.name1);
        } else {
          var added = FavoritesService.prospects.addFavoriteItems($scope.selectedClient.name1);
        }

        if (added) {
          window.plugins.toast.showWithOptions({
            message: "Added To Favorites",
            duration: "short", // which is 2000 ms. "long" is 4000. Or specify the nr of ms yourself.
            position: "center",
            addPixelsY: -40 // added a negative value to move it up a bit (default 0)
          });
        }
      }
    } // End of Toggle Like Button


    $scope.goBack = function() {
      $state.go('app.client_search_result');
    }

    $scope.goToSearchForm = function() {
      $state.go('app.client_search');
    }

    $scope.goToMapPage = function() {
      $state.go('tab.map');
    }
  })



  .controller('ClientHistoryFilterCtrl', function($scope, $ionicPlatform, $http) {
    /*   console.log("Inside Client History Filter Control");
      $scope.active_segment = 1;
     $scope.activate_segment = function (index) {
                $scope.active_segment = index;
                $ionicScrollDelegate.resize();
            };
     
     var doCustomBack= function() {
    alert("Hardware Back Button Pressed");
};

// registerBackButtonAction() returns a function which can be used to deregister it
var deregisterHardBack= $ionicPlatform.registerBackButtonAction(
    doCustomBack, 101
);

$scope.$on('$destroy', function() {
    deregisterHardBack();
}); */
    var url = "feeds/clients_list.json";
    $http.get(url).then(function(result) {
      $scope.myClients = result.data;

    })

  })

  .controller('SupportCtrl', function($scope, $ionicPlatform, $rootScope) {
   $scope.$on('$ionicView.enter', function() {
    $crisp.push(["set", "user:email",  $rootScope.useremail]);
     if ($crisp.is("chat:hidden")) {
       $crisp.push(['do', 'chat:show']);
     }
   });

   $scope.support = { version: null, email: null, website: null, websitename: null };
   $scope.support.website = "https://app.davey.com/DaveyMobile";
   $scope.support.email = "sourav.das@davey.com?subject=Feedback Regarding Davey App";
   $scope.support.websitename = "Davey Mobile Landing Page";
   $scope.support.contact = "(330)6739515 x8555";
   cordova.getAppVersion.getVersionNumber().then(function(version) {
     $scope.version = version;
   });

   $scope.openAppStore = function() {
     if (ionic.Platform.isAndroid()) {
       window.open("market://details?id=com.daveymobilev101005", "_system");
     }
   }

   $scope.openChat = function() {
     $crisp.push(['do', 'chat:open']);
   }

   $scope.$on('$ionicView.beforeLeave', function(e) {
     $crisp.push(['do', 'chat:hide']);
   });

 })


  .controller('PlaylistsCtrl', function($scope) {
    $scope.playlists = [{
      title: 'Reggae',
      id: 1
    }, {
      title: 'Chill',
      id: 2
    }, {
      title: 'Dubstep',
      id: 3
    }, {
      title: 'Indie',
      id: 4
    }, {
      title: 'Rap',
      id: 5
    }, {
      title: 'Cowbell',
      id: 6
    }];

  })
