angular.module('starter.routes', [])
.constant('routesConfig', (function () {
		'use strict';

		var rootRoutesConfig = {
			categories: 'feeds/categories.json',
			items: 'feeds/notifications.json'
		};

		var routesConfig = {
			
			categories: {
				all: function () {
					return rootRoutesConfig.categories;
				}
			},

			items: {
				all: function () {
					return rootRoutesConfig.items;
				}
			}
			
		}

		return routesConfig;
	})());