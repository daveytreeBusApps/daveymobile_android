angular.module('starter.filters', [])
.filter('trustHtml', [
		'$sce',
		function ($sce) {
			'use strict';

			return function (input) {
				return $sce.trustAsHtml(input);
			};
		}
	])

.filter('asArray', [function () {
	return function (contents) {
		return Array.isArray(contents) ? contents : [contents];
	}
}])

.filter('asAffectedArray', [function () {
	return function (contents) {
		if(Array.isArray(contents)){
			return contents;
		}
		else{
			var keys = Object.keys(contents);
           // var values = keys.map(key => contents[key]);
           var values = keys.map(function(key){return contents[key];});
            return values;
		}
		
	}
}])

.filter('asMonthName', [function(){
var monthNames = [null, 'JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];

     return function(monthNumber){
        if(monthNumber > 0 && monthNumber < monthNames.length){
        return monthNames[monthNumber];
      }
       else{
     return '';
  }
};
}])

.filter('asMonthName2', [function(){
var monthNames = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];

     return function(monthNumber){
        if(monthNumber > 0 && monthNumber < monthNames.length){
        return monthNames[monthNumber];
      }
       else{
     return '';
  }
};
}])

.filter('asFullTypeString', [function(){
     return function(custType){
        if(custType == 'C'){
        return 'CUSTOMER';
      }
       if(custType == 'P'){
        return 'PROSPECT';
      }
       else{
     return '';
      }
};
}])


.filter('isLocationListed', [function(){
	return function(location, species){
		console.log("isLocationListed", Location);
		for(var item_counter = 0;item_counter < species.length ; item_counter++ ){
			if(species[item_counter].location == location.LocationCode){
				return location;
				break;
			}
		}

	}

}])

.filter('isSpeciesListed', [function(){
	return function(species, location){
		var species_list = [];
		for(var item_counter = 0;item_counter < species.length ; item_counter++){
			if(location == species[item_counter].location){
			species_list.push(species[item_counter]);
		 }
		}
		return species_list;
		
	}

}])

.filter('capitalize', function () {
    return function (input) {
      var newInput = input.toLowerCase();
      var words = newInput.split(' ');
      angular.forEach(words, function (word, i) {
        words[i] = words[i].charAt(0).toUpperCase() + words[i].slice(1);
      });

      return words.join(' ');
    };
  })

.filter('asOneDecimal', [function(){
	return function(content){
		var value = parseFloat(content).toFixed(2);
		if(value % 1 == 0){
			return parseInt(value);
		}
		else{
			return value;
		}
		
	}
}])

.filter('prettifyUrl', [function(){
    return function prettifyUrl(url) {
        if (typeof url !== 'string' || url === '') {
            return null;
        }

        if (url.indexOf('http://') === 0) {
            url = url.substring(7);
        } else if (url.indexOf('https://') === 0) {
            url = url.substring(8);
        }

        if (/\/$/.test(url)) {
            return url.substring(0, url.length - 1);
        }

        return url;
    };


}])

.filter('phoneNumberFormatted', [function(){
  return function(content){
		   var outString = content.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
		 var number = outString.replace(/\s/g, '');
		 if (!number) { return ''; }

		 number = String(number);

		 // Will return formattedNumber. 
		 // If phonenumber isn't longer than an area code, just show number
		 var formattedNumber = number;

		 // if the first character is '1', strip it out and add it back
		 var c = (number[0] == '1') ? '1 ' : '';
		 number = number[0] == '1' ? number.slice(1) : number;

		 // # (###) ###-#### as c (area) front-end
		 var area = number.substring(0, 3);
		 var front = number.substring(3, 6);
		 var end = number.substring(6, 10);

		 if (front) {
		   formattedNumber = (c + "(" + area + ") " + front);
		 }
		 if (end) {
		   formattedNumber += ("-" + end);
		 }
		 return formattedNumber;


		  }

}])

.filter('dateFormatted', [function(){
    return function(content){
    	var formattedDate = new Date(content);
    	return formattedDate;
    }

}]);

  





